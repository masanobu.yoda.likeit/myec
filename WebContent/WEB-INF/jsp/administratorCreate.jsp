<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>管理者情報新規登録画面</title>
<link rel="stylesheet" href="css/original/common.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

<jsp:include page="/baselayout/header2.jsp" />

<div class="container">

<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
</c:if>

<form action="administratorCreate" method="post" class="form-horizontal">

  <div class="form-group row">
    <label for="loginId" class="col-sm-2 col-form-label">ログインID</label>
    <div class="col-sm-10">
    <input type="text" class="form-control" id="inputLoginId" name="loginId" value="${loginId}">
    </div>
  </div>

  <div class="form-group row">
    <label for="InputPassword" class="col-sm-2 col-form-label">パスワード</label>
    <div class="col-sm-10">
    <input type="password" class="form-control" id="inputPassword" name="password">
    </div>
  </div>

  <div class="form-group row">
    <label for="InputPassword" class="col-sm-2 col-form-label">パスワード(確認)</label>
    <div class="col-sm-10">
    <input type="password" class="form-control" id="inputPassword" name="passwordConf">
    </div>
  </div>

  <div class="form-group row">
    <label for="userName" class="col-sm-2 col-form-label">ユーザー名</label>
    <div class="col-sm-10">
    <input type="text" class="form-control" id="userName" name="name" value="${name}">
    </div>
  </div>

  <div class="form-group row">
    <label for="birthdate" class="col-sm-2 col-form-label">生年月日</label>
    <div class="col-sm-10">
    <input type="date" class="form-control" id="birthdate" name="birthdate" value="${birthdate}">
    </div>
  </div>


  <div class="submit-button-area">
  <button type="submit" value="検索" class="btn btn-primary btn-lg btn-block">登録</button>
  </div>

  <div class="col-sm-4">
  <a href="administratorList">戻る</a>
  </div>

</form>

</div>
</body>
</html>