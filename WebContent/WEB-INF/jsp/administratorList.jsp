<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>管理者一覧画面</title>
<link rel="stylesheet" href="css/original/common.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

<jsp:include page="/baselayout/header2.jsp" />


<div class="container">
<div class="create-button-area">
 <a href="administratorCreate" class="btn btn-outline-primary btn-lg">新規登録</a>
</div>

<div class="search-form-area">
<form method="post" action="administratorList" class="form-horizontal">
  <div class="form-group row">
    <label for="loginId" class="col-sm-2 col-form-label">ログインID</label>
    <div class="col-sm-10">
    <input type="text" class="form-control" id="user-id" name="login_id" value="${login_id}">
    </div>
  </div>

  <div class="form-group row ">
    <label for="userName" class="col-sm-2 col-form-label">ユーザー名</label>
    <div class="col-sm-10">
    <input type="text" class="form-control" id="userName" name="name" value="${name}">
    </div>
  </div>

  <div class="form-group row">
    <label for="birthDate" class="col-sm-2 col-form-label">生年月日</label>
    <div class="row col-sm-10">
    <div class="col-sm-5">
      <input type="date" name="birthdate" id="date-start" class="form-control" value="${birthdate}">
    </div>
    <div class="col-sm-1 text-cemter">～</div>
    <div class="col-sm-5">
      <input type="date" name="birthdate2" id="date-end" class="form-control" value="${birthdate2}">
    </div>
    </div>
  </div>

  <div class="text-right">
    <button type="submit" class="btn btn-primary">検索</button>
  </div>

</form>
</div>



<div class="table-responsive">
<table class="table table-striped">
  <thead class="thead-dark">
    <tr>
      <th scope="col">ログインID</th>
      <th scope="col">ユーザー名</th>
      <th scope="col">生年月日</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
   <c:forEach var="a" items="${aList}">
    <tr>
      <td>${a.loginId}</td>
      <td>${a.name}</td>
      <td>${a.birthDate}</td>
      <td>
       <a class="btn btn-primary" href="administratorDetail?id=${a.id}">詳細</a>
       <a class="btn btn-success" href="administratorUpdate?id=${a.id}">更新</a>
       <a class="btn btn-danger" href="administratorDelete?id=${a.id}">削除</a>
      </td>
    </tr>
   </c:forEach>
  </tbody>
</table>
</div>
</div>


</body>
</html>