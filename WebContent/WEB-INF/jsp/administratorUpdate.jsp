<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>管理者情報更新画面</title>
<link rel="stylesheet" href="css/original/common.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

<jsp:include page="/baselayout/header2.jsp" />


<div class="container">

<form method="post" action="administratorUpdate" class="form-horizontal">

<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
</c:if>

  <div class="form-group row">
    <label for="loginId" class="col-sm-2 col-form-label">ログインID</label>
    <div class="col-sm-10">
    <p class="form-control-plaintext">${ad.loginId }</p>
    </div>
  </div>

  <div class="form-group row">
    <label for="password" class="col-sm-2 col-form-label">パスワード</label>
    <div class="col-sm-10">
    <input type="password" class="form-control" id="password" name="password">
    </div>
  </div>

  <div class="form-group row">
    <label for="password" class="col-sm-2 col-form-label">パスワード(確認)</label>
    <div class="col-sm-10">
    <input type="password" class="form-control" id="passwordConf" name="passwordConf">
    </div>
  </div>

  <div class="form-group row">
    <label for="userName" class="col-sm-2 col-form-label">ユーザー名</label>
    <div class="col-sm-10">
    <input type="text" class="form-control" id="userName" value="${ad.name}" name="name">
    </div>
  </div>

   <div class="form-group row">
    <label for="userName" class="col-sm-2 col-form-label">生年月日</label>
    <div class="col-sm-10">
    <input type="date" class="form-control" id="birthDate" value="${ad.birthDate}" name="birth_date">
    </div>
  </div>

  <div class="submit-button-area">
  <input type="hidden" name="id" value="${ad.id }">
  <input type="submit" value="更新" class="btn btn-primary btn-lg btn-block">
  </div>

  <div class="col-xs-4">
    <a href="administratorList">戻る</a>
  </div>

</form>
</div>

</body>
</html>