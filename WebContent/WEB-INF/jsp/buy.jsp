<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>購入</title>
<link rel="stylesheet" href="css/original/common.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

<jsp:include page="/baselayout/header.jsp" />

<div class="container">
<br><br>
<div class="text-center">
 <h4>カートアイテム</h4>
</div>

<br><br>

<form action="buyconfirm" method="post">
<table class="table table-striped ">
  <thead class="thead-dark">
    <tr>
      <th scope="col">商品名</th>
      <th scope="col">単価</th>
      <th scope="col">小計</th>
    </tr>
  </thead>
  <tbody>
   <c:forEach var="cartInItem" items="${cart}">
    <tr>
      <th scope="row">${cartInItem.name }</th>
      <td>${cartInItem.formatPrice }円</td>
      <td>${cartInItem.formatPrice }円</td>
    </tr>
    </c:forEach>
    <tr>
      <th scope="row"></th>
      <td></td>
      <td>
        <div class="input-field col s8 offset-s2">
          <select name="delivery_method_id">
            <c:forEach var="dmdb" items="${dmdbList }">
              <option value="${dmdb.id }">${dmdb.name }</option>
            </c:forEach>
          </select>
        </div>
      </td>
    </tr>
  </tbody>
</table>

<br><br><br>

<div class="text-center">
   <button type="submit" value="検索" class="btn btn-primary btn-lg " name="action">購入確認</button>
  </div>

</form>
</div>

</body>
</html>