<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>購入確認</title>
<link rel="stylesheet" href="css/original/common.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

<jsp:include page="/baselayout/header.jsp" />


<div class="container">
<br><br>
<div class="text-center">
 <h4>購入</h4>
</div>

<br><br>

<table class="table table-striped ">
  <thead class="thead-dark">
    <tr>
      <th scope="col">商品名</th>
      <th scope="col">単価</th>
      <th scope="col">小計</th>
    </tr>
  </thead>
  <tbody>
     <c:forEach var="cartInItem" items="${cart }">
        <tr>
         <td>${cartInItem.name }</td>
         <td>${cartInItem.formatPrice } 円</td>
         <td>${cartInItem.formatPrice } 円</td>
        </tr>
    </c:forEach>
    <tr>
      <td>${bdb.deliveryMethodName }</td>
      <td></td>
      <td>${bdb.deliveryMethodPrice } 円</td>
    </tr>
    <tr>
      <td></td>
      <td>合計</td>
      <td>${bdb.formatTotalPrice} 円</td>
    </tr>
  </tbody>
</table>

<br><br><br>

  <div class="text-center">
    <form action="buyresult" method="post">
      <button type="submit" value="検索" class="btn btn-primary btn-lg " >購入</button>
    </form>
  </div>


</div>


</body>
</html>