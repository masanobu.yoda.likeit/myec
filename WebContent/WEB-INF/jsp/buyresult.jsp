<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>購入完了</title>
<link rel="stylesheet" href="css/original/common.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

<jsp:include page="/baselayout/header.jsp" />

<div class="container">
<br><br>
<div class="text-center">
 <h4>購入が完了しました</h4>
</div>

<br>

<form action="buyconfirm" method="post">

<div class="form-group row">
   <div class="col-sm-6">
   <a href="index"  class="btn btn-primary btn-block">引き続き買い物をする</a>
   </div>
   <div class="col-sm-6">
   <a href="userdata" class="btn btn-primary btn-block">ユーザー情報へ</a>
   </div>
  </div>

<br>

<div class="text-center">
 <h4>購入詳細</h4>
</div>

<br><br>

<table class="table table-striped ">
  <thead class="thead-dark">
    <tr>
      <th scope="col">購入日時</th>
      <th scope="col">配送方法</th>
      <th scope="col">合計金額</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>${resultBDB.formatDate }</th>
      <td>${resultBDB.deliveryMethodName }</td>
      <td>${resultBDB.formatTotalPrice }</td>
    </tr>
  </tbody>
</table>

<br>

<table class="table table-striped ">
  <thead class="thead-dark">
    <tr>
      <th scope="col">商品名</th>
      <th scope="col">単価</th>
    </tr>
  </thead>
  <tbody>
   <c:forEach var="buyIDB" items="${buyIDBList }">
    <tr>
      <th scope="row">${buyIDB.name }</th>
      <td>${buyIDB.formatPrice }円</td>
    </tr>
    </c:forEach>
    <tr>
      <th scope="row">${resultBDB.deliveryMethodName }</th>
      <td>${resultBDB.deliveryMethodPrice }円</td>
    </tr>
  </tbody>
</table>
</form>
</div>

</body>
</html>