<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>カート確認</title>
<link rel="stylesheet" href="css/original/common.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body>

<jsp:include page="/baselayout/header.jsp" />

 <div class="container">
  <div class="text-center">
  ${cartActionMessage}
  <h4>買い物かご</h4>
  </div>

<br>
<br>
  <div class="section">
  <form action="itemdelete" method="post">
  <div class="row">
     <div class="col-sm-6">
       <button class="btn btn-primary btn-block" type="submit" name="action" >削除</button>
     </div>
     <div class="col-sm-6">
       <a href="buy" class="btn btn-primary btn-block">レジに進む</a>
     </div>
   </div>


<br><br><br>

 <div class="row">
  <c:forEach var="item" items="${cart }"  varStatus="status">
  <div class="col-sm-3">
   <div class="card">
    <div class="card-image">
      <a href="item?item_id=${item.id }"><img class="item-image" src="img/${item.fileName }"></a>
    </div>
    <div class="card-content">
      <span class="card-title">${item.name}</span>
      <p>${item.formatPrice}円</p>
      <p><input type="checkbox" id="${status.index }" name="delete_item_id_list" value="${item.id }"><label for="${status.index}">削除</label></p>
    </div>
   </div>
  </div>
  </c:forEach>
 </div>
 <div class="row">

 </div>


</form>
</div>
</div>

</body>
</html>