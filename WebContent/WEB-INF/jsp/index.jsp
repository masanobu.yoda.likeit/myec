<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>TOPページ</title>
<link rel="stylesheet" href="css/original/common.css">
<link rel="stylesheet" href="css/original/common2.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

<jsp:include page="/baselayout/header.jsp" />

 <div class="container">
 <div class="container-inner">


 <h1 class="text-center">書籍販売サイト</h1>

 <br>

   <div class="text-center">
    <h5>初めてご利用ですか？</h5>

   </div>

   <br>

   <form action="itemsearchresult" method="get">
   <div class="form-group row">
    <button class="col-sm-2 col-form-label btn btn-primary" >商品検索</button>
    <div class="col-sm-10">
    <input type="text" class="form-control" id="search" name="search_word">
    </div>
   </div>
   </form>

 </div>
 </div>

<br><br><br><br>

 <div class="container">
  <div class="container-inner">

  <div class="text-center">
   <h3>おすすめ商品一覧</h3>
  </div>

<br><br>

  <div class="row">
  <c:forEach var="item" items="${itemList}">
   <div class="col-sm-3">
    <div class="card">
     <div class="card-image">
     <a href="item?item_id=${item.id}">
      <img class="item-image" src="img/${item.fileName}">
     </a>
     </div>
     <div class="card-content">
      <span class="card-title">${item.name}</span>
      <p>${item.formatPrice}円</p>
     </div>
    </div>
   </div>
  </c:forEach>
  </div>

 </div>
 </div>

</body>
</html>