<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>管理者用TOPページ</title>
<link rel="stylesheet" href="css/original/common.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

<jsp:include page="/baselayout/header2.jsp" />

 <div class="container">

 <div class="create-button-area">
    <a href="itemdatacreate" class="btn btn-outline-primary btn-lg">新規登録</a>
 </div>

<form method="post" action="index2" class="form-horizontal">

 <div class="search-form-area">

  <div class="form-group row">
    <label for="loginId" class="col-sm-2 col-form-label">ID</label>
    <div class="col-sm-10">
    <input type="text" class="form-control" id="inputLoginId" name="id" value="${id}">
    </div>
  </div>

  <div class="form-group row">
    <label for="itemName" class="col-sm-2 col-form-label">商品名</label>
    <div class="col-sm-10">
    <input type="text" class="form-control" id="itemName" name="name" value="${name}">
    </div>
  </div>

  <div class="form-group row">
    <label for="birthDate" class="col-sm-2 col-form-label">価格</label>
    <div class="col-sm-10 row">
      <div class="col-sm-5 ">
      <input type="number" class="form-control" id="date-start" name="price" value="${price}">
      </div>
      <div class="col-sm-1 text-center">～</div>
      <div class="col-sm-5 ">
      <input type="number" class="form-control" id="date-end" name="price2" value="${price2}">
      </div>
    </div>
  </div>

  <div class="form-group row">
    <label for="itemData" class="col-sm-2 col-form-label">詳細情報</label>
    <div class="col-sm-10">
    <input type="text" class="form-control" id="itemData" name="detail" value="${detail}">
    </div>
  </div>



  <div class="text-right">
  <button type="submit" class="btn btn-primary">検索</button>
  </div>

 </div>

</form>

<table class="table table-striped ">
  <thead class="thead-dark">
    <tr>
      <th scope="col">ID</th>
      <th scope="col">商品名</th>
      <th scope="col">価格</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
   <c:forEach var="i" items="${iList}">
    <tr>
      <td>${i.id}</td>
      <td>${i.name}</td>
      <td>${i.price}円</td>
      <td>
       <a class="btn btn-primary" href="itemdatadetail?id=${i.id}">詳細</a>
       <a class="btn btn-success" href="itemdataupdate?id=${i.id}">更新</a>
       <a class="btn btn-danger" href="itemdatadelete?id=${i.id}">削除</a>
      </td>
    </tr>
   </c:forEach>
  </tbody>
</table>
</div>

</body>
</html>