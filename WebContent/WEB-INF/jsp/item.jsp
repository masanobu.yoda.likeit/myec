<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>商品詳細</title>
<link rel="stylesheet" href="css/original/common.css">
<link rel="stylesheet" href="css/original/common2.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>

<jsp:include page="/baselayout/header.jsp" />


<br>

 <div class="container">
  <div class="container-inner">

  <div class="row center">
   <div class="col s4"></div>
   <div class="col s4"><h3 class="col s12 light">商品詳細</h3></div>
   <div class="col s4">
    <form action="itemadd" method="post">
     <input type="hidden" name="item_id" value="${item.id}">
     <button class="btn btn-primary  btn-lg btn-block" type="submit" name="action">買い物かごに追加</button>
    </form>
   </div>
  </div>
 <br>
 <br>

 <div class="row">
  <div class="col-sm-6">
   <div class="card">
    <div class="card-image">
      <img src="img/${item.fileName}" width="520" height="600">
    </div>
   </div>
  </div>

  <div class="col-sm-6">
   <h4>${item.name }</h4>
   <h5>${item.formatPrice } 円</h5>
   <br>
   <p>${item.detail} </p>
  </div>

 </div>

 </div>
 </div>
</body>
</html>