<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>商品情報新規登録画面</title>
<link rel="stylesheet" href="css/original/common.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

<jsp:include page="/baselayout/header2.jsp" />


<div class="container">

<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
</c:if>

<form action="itemdatacreate" method="post">

  <div class="form-group row">
    <label for="itemName" class="col-sm-2 col-form-label">商品名</label>
    <div class="col-sm-10">
    <input type="text" class="form-control" id="itemName" name="name">
    </div>
  </div>

 <div class="form-group row">
    <label for="itemPrice" class="col-sm-2 col-form-label">価格</label>
    <div class="col-sm-10">
    <input type="text" class="form-control" id="itemPrice" name="price">
    </div>
  </div>

  <div class="form-group row">
    <label for="itemData" class="col-sm-2 col-form-label">詳細情報</label>
    <div class="col-sm-10">
    <input type="text" class="form-control" id="itemData" name="detail">
    </div>
  </div>

  <div class="form-group row">
    <label for="fileName" class="col-sm-2 col-form-label">ファイル名</label>
    <div class="col-sm-10">
    <input type="text" class="form-control" id="fileName" name="file_name">
    </div>
  </div>

  <div class="submit-button-area">
  <button type="submit" value="検索" class="btn btn-primary btn-lg btn-block">登録</button>
  </div>

  <div class="col-sm-4">
  <a href="index2">戻る</a>
  </div>

</form>

</div>


</body>
</html>