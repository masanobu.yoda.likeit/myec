<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>商品情報詳細画面</title>
<link rel="stylesheet" href="css/original/common.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

<jsp:include page="/baselayout/header2.jsp" />


<div class="container">

<form method="post" action="#">

<div class="form-group row">
 <label for="loginId" class="col-sm-2 col-form-label">ID</label>
 <div class="col-sm-10">
 <p class="form-control-plaintext">${ID.id}</p>
 </div>
</div>

<div class="form-group row">
 <label for="itemName" class="col-sm-2 col-form-label">商品名</label>
 <div class="col-sm-10">
 <p class="form-control-plaintext">${ID.name}</p>
 </div>
</div>

<div class="form-group row">
 <label for="itemPrice" class="col-sm-2 col-form-label">価格</label>
 <div class="col-sm-10">
 <p class="form-control-plaintext">${ID.price }</p>
 </div>
</div>

<div class="form-group row">
 <label for="itemDate" class="col-sm-2 col-form-label">詳細情報</label>
 <div class="col-sm-10">
 <p class="form-control-plaintext">${ID.detail }</p>
 </div>
</div>

<div class="form-group row">
 <label for="fileName" class="col-sm-2 col-form-label">ファイル名</label>
 <div class="col-sm-10">
 <p class="form-control-plaintext">${ID.fileName }</p>
 </div>
</div>

<div class="form-group row">
 <label for="fileName" class="col-sm-2 col-form-label">画像</label>
 <div class="col-sm-5">
 <div class="card">
    <div class="card-image">
      <img src="img/${ID.fileName} " width="520" height="600">
    </div>
   </div>
 </div>
</div>

<div class="col-xs-4">
<a href="index2">戻る</a>
</div>

</form>

</div>


</body>
</html>