<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>商品情報更新画面</title>
<link rel="stylesheet" href="css/original/common.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

<jsp:include page="/baselayout/header2.jsp" />


<div class="container">

<form method="post" action="itemdataupdate" class="form-horizontal">

<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
</c:if>

  <div class="form-group row">
    <label for="password" class="col-sm-2 col-form-label">商品名</label>
    <div class="col-sm-10">
    <input type="text" class="form-control" id="name" name="name" value="${ID.name}">
    </div>
  </div>

  <div class="form-group row">
    <label for="password" class="col-sm-2 col-form-label">価格</label>
    <div class="col-sm-10">
    <input type="number" class="form-control" id="price" name="price" value="${ID.price}">
    </div>
  </div>

  <div class="form-group row">
    <label for="itemData" class="col-sm-2 col-form-label">詳細情報</label>
    <div class="col-sm-10">
    <input type="text" class="form-control" id="detail" name="detail" value="${ID.detail}">
    </div>
  </div>

   <div class="form-group row">
    <label for="fileName" class="col-sm-2 col-form-label">ファイル名</label>
    <div class="col-sm-10">
    <input type="text" class="form-control" id="file_name" name="file_name" value="${ID.fileName}">
    </div>
  </div>

  <div class="submit-button-area">
  <input type="hidden" name="id" value="${ID.id}">
  <input type="submit" value="更新" class="btn btn-primary btn-lg btn-block">
  </div>

  <div class="col-xs-4">
    <a href="index2">戻る</a>
  </div>

</form>
</div>


</body>
</html>