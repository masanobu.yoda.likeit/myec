<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>検索結果</title>
</head>
<link rel="stylesheet" href="css/original/common.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<body>

<jsp:include page="/baselayout/header.jsp" />

<br>

 <div class="container">
     <form action="itemsearchresult" method="get">
     <div class="form-group row">
      <button class="col-sm-2 col-form-label btn btn-primary" type="submit">検索</button>
      <div class="col-sm-10">
      <input type="text" class="form-control" id="search" name="search_word" value="${searchWord }">
      </div>
      </div>
     </form>
    </div>

<br>

 <div class="container">
  <div class="text-center">
  <h3 class="col s12 light">検索結果</h3>
  <p>検索結果${itemCount}件</p>
  </div>

  <br>

  <div class="section">
     <!-- 商品情報 -->
     <div class="row">
        <c:forEach var="item" items="${itemList }" varStatus="status">
         <div class="col-sm-3">
          <div class="card">
             <div class="card-image">
                <a href="item?item_id=${item.id }&page_num=${pageNum}"><img class="item-image" src="img/${item.fileName}"></a>
             </div>
             <div>
                <span class="card-title">${item.name }</span>
                <p>${item.formatPrice }円</p>
             </div>
          </div>
          </div>
          <br>
        </c:forEach>
     </div>

    <br><br>

			<ul class="pagination pagination-lg justify-content-center">
				<!-- １ページ戻るボタン  -->
				<c:choose>
					<c:when test="${pageNum == 1}">
						<li class="page-item disabled"><a class="page-link">前</a></li>
					</c:when>
					<c:otherwise>
						<li class="page-item waves-effect"><a class="page-link" href="itemsearchresult?search_word=${searchWord}&page_num=${pageNum - 1}">前</a></li>
					</c:otherwise>
				</c:choose>

				<!-- ページインデックス -->
				<c:forEach begin="${(pageNum - 5) > 0 ? pageNum - 5 : 1}" end="${(pageNum + 5) > pageMax ? pageMax : pageNum + 5}" step="1" varStatus="status">
					<li <c:if test="${pageNum == status.index }"> class="active" </c:if>><a class="page-link" href="itemsearchresult?search_word=${searchWord}&page_num=${status.index}">${status.index}</a></li>
				</c:forEach>

				<!-- 1ページ送るボタン -->
				<c:choose>
				<c:when test="${pageNum == pageMax || pageMax == 0}">
					<li class="page-item disabled"><a class="page-link">次</a></li>
				</c:when>
				<c:otherwise>
					<li class="page-item waves-effect"><a class="page-link" href="itemsearchresult?search_word=${searchWord}&page_num=${pageNum + 1}">次</a></li>
				</c:otherwise>
				</c:choose>
			</ul>
		</nav>


 </div>
 </div>

</body>
</html>