<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン</title>
<link rel="stylesheet" href="css/original/common.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>



<br><br>

<h4 class="text-center">書籍販売サイトログインページ</h4>

<div class="container">

 <form method="post" action="login" >

   <c:if test="${errMsg != null}">
    <div class="alert alert-danger" role="alert">
		  ${errMsg}
    </div>
    </c:if>

  <div class="form-group">
    <label></label>
    <input type="text" class="form-control" name="login_id" id="inputLogiinId"  placeholder="ログインID">
  </div>
  <div class="form-group">
    <label></label>
    <input type="password" class="form-control" name="password" id="exampleInputPassword1" placeholder="Password">
  </div>
  <div class="form-group">
    <label></label>
  <button type="submit" class="btn btn-primary btn-lg btn-block">ログイン</button>
  </div>
  <br>
  <div class="form-group ">
   <div class="col s8 offset-s2">
    <p class="right-align">
    <a href="regist">新規登録</a>
    </p>
   </div>
  </div>
  <div class="form-group ">
   <div class="col s8 offset-s2">
    <p class="right-align">
    <a href="login2">管理者用</a>
    </p>
   </div>
  </div>
</form>

</div>

</body>
</html>