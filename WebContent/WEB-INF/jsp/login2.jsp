<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>管理者用ログイン</title>
<link rel="stylesheet" href="css/original/common.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>



<br><br>

<h4 class="text-center">書籍販売サイト管理者用ログインページ</h4>

<div class="container">

 <form method="post" action="login2" class="form-signin">

  <c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>

  <div class="form-group">
    <label></label>
    <input type="text" class="form-control" id="inputLogiinId"  placeholder="ログインID" name="loginId">
  </div>
  <div class="form-group">
    <label></label>
    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password">
  </div>

  <br>

  <div class="form-group row">
    <div class="col-sm-6">
    <a href="login"  class="btn btn-primary btn-lg btn-block">戻る</a>
    </div>
    <div class="col-sm-6">
    <button type="submit" class="btn btn-primary btn-lg btn-block">ログイン</button>
    </div>
  </div>
 </form>

</div>

</body>
</html>