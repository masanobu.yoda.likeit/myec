<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログアウト</title>
<link rel="stylesheet" href="css/original/login.css">
<link rel="stylesheet" href="css/original/common.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>



<div class="container">

<form method="post" action="login">

<br><br>

<div class="text-center">
 <h4>ログアウトしました</h4>
</div>

<br>

<div class="form-group row">
   <a class="btn btn-primary btn-lg btn-block" href="login" role="button">ログインページへ</a>
</div>


</form>

</div>


</body>
</html>