<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー削除画面</title>
<link href="css/original/common.css" rel="stylesheet">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

<div class="container">

<form method="post" action="taikai" class="form-horizontal" >


<div class="delete-area">
<p>ログインID:${userList.id }様、本当に退会してよろしいでしょうか？</p>

<div class="row">
<div class="col-sm-6">
<a href="index" class="btn btn-primary btn-block" >いいえ</a>
</div>

<div class="col-sm-6">
<input type="hidden" name="id" value="${userList.id }">
<input type="submit"  class="btn btn-light btn-block" value="はい">

</div>
</div>
</div>
</form>
</div>


</body>
</html>