<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー詳細画面</title>
<link rel="stylesheet" href="css/original/common.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>


<jsp:include page="/baselayout/header2.jsp" />


<div class="container">

<form method="post" action="#">

<div class="form-group row">
 <label for="loginId" class="col-sm-2 col-form-label">ログインID</label>
 <div class="col-sm-10">
 <p class="form-control-plaintext">${user.loginId}</p>
 </div>
</div>

<div class="form-group row">
 <label for="userName" class="col-sm-2 col-form-label">ユーザー名</label>
 <div class="col-sm-10">
 <p class="form-control-plaintext">${user.name }</p>
 </div>
</div>

<div class="form-group row">
 <label for="birthDate" class="col-sm-2 col-form-label">生年月日</label>
 <div class="col-sm-10">
 <p class="form-control-plaintext">${user.birthDate }</p>
 </div>
</div>

<div class="form-group row">
 <label for="createDate" class="col-sm-2 col-form-label">新規登録日時</label>
 <div class="col-sm-10">
 <p class="form-control-plaintext">${user.createDate }</p>
 </div>
</div>

<div class="form-group row">
 <label for="updateDate" class="col-sm-2 col-form-label">更新日時</label>
 <div class="col-sm-10">
 <p class="form-control-plaintext">${user.updateDate}</p>
 </div>
</div>

<br><br><br><br>

<table class="table table-striped ">
  <thead class="thead-dark">
    <tr>
      <th scope="col">購入日時</th>
      <th scope="col">配送方法</th>
      <th scope="col">購入金額</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
    <c:forEach var="bd" items="${bdList}">
    <tr>
      <th scope="row">${bd.formatDate}</th>
      <td>${bd.deliveryMethodName}</td>
      <td>${bd.formatTotalPrice}円</td>
      <td>
         <a class="btn btn-primary" href="userbuyhistorydetail2?buy_id=${bd.id }">詳細</a>
      </td>
    </tr>
    </c:forEach>
  </tbody>
</table>


<div class="col-xs-4">
<a href="userList">戻る</a>
</div>

</form>

</div>

</body>
</html>