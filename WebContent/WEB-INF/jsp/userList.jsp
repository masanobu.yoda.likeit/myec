<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1">
<title>ユーザー一覧画面</title>
<link rel="stylesheet" href="css/original/common.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

<jsp:include page="/baselayout/header2.jsp" />


<div class="container">

<form method="post" action="userList" class="form-horizontal">

 <div class="search-form-area">

  <div class="form-group row">
    <label for="loginId" class="col-sm-2 col-form-label">ログインID</label>
    <div class="col-sm-10">
    <input type="text" class="form-control" id="inputLoginId" name="loginId" value="${loginId}">
    </div>
  </div>

  <div class="form-group row">
    <label for="userName" class="col-sm-2 col-form-label">ユーザー名</label>
    <div class="col-sm-10">
    <input type="text" class="form-control" id="userName" name="name" value="${name}">
    </div>
  </div>

  <div class="form-group row">
    <label for="birthDate" class="col-sm-2 col-form-label">生年月日</label>
    <div class="col-sm-10 row">
      <div class="col-sm-5 ">
      <input type="date" class="form-control" id="date-start" name="birthdate" value="${birthdate}">
      </div>
      <div class="col-sm-1 text-center">～</div>
      <div class="col-sm-5 ">
      <input type="date" class="form-control" id="date-end" name="birthdate2" value="${birthdate2}">
      </div>
    </div>
  </div>

  <div class="text-right">
  <button type="submit" class="btn btn-primary">検索</button>
  </div>

 </div>

</form>

<table class="table table-striped ">
  <thead class="thead-dark">
    <tr>
      <th scope="col">ログインID</th>
      <th scope="col">ユーザー名</th>
      <th scope="col">生年月日</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
   <c:forEach var="user" items="${userList}">
    <tr>
      <td>${user.loginId}</td>
      <td>${user.name}</td>
      <td>${user.birthDate}</td>
      <td>
         <a class="btn btn-primary" href="userDetail?id=${user.id}">詳細</a>
      </td>
    </tr>
    </c:forEach>
  </tbody>
</table>
</div>
</body>
</html>