<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>購入履歴削除</title>
<link rel="stylesheet" href="css/original/common.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

<jsp:include page="/baselayout/header.jsp" />

<div class="container">
 <form method="post" action="userdata">
   <div class="delete-area">
   <p>本当に削除してよろしいでしょうか？</p>
   <div class="row">
     <div class="col-sm-6">
       <a href="userdata" class="btn btn-primary btn-block">いいえ</a>
     </div>
     <div class="col-sm-6">
       <input type="hidden" name="buy_id" value="${resultBDB.id}">
       <input type="submit" class="btn btn-primary btn-block" value="はい">
     </div>
   </div>
   </div>
 </form>
</div>

</body>
</html>