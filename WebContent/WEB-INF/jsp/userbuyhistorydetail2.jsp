<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー購入詳細</title>
<link rel="stylesheet" href="original/common.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

<jsp:include page="/baselayout/header2.jsp" />

<br><br>

<div class="container">
 <br>
  <div class="text-center"><h2>購入詳細</h2></div>
 <br>
 <table class="table table-striped ">
  <thead class="thead-dark">
    <tr>
      <th scope="col">購入日時</th>
      <th scope="col">配送方法</th>
      <th scope="col">購入金額</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">${resultBDB.formatDate }</th>
      <td>${resultBDB.deliveryMethodName }</td>
      <td>${resultBDB.formatTotalPrice }</td>
    </tr>
  </tbody>
</table>

<br>

<table class="table table-striped ">
  <thead class="thead-dark">
    <tr>
      <th scope="col">商品名</th>
      <th scope="col">単価</th>
    </tr>
  </thead>
  <tbody>
   <c:forEach var="buyIDB" items="${buyIDBList }">
    <tr>
      <th scope="row">${buyIDB.name}</th>
      <td>${buyIDB.formatPrice}円</td>
    </tr>
    </c:forEach>
    <tr>
      <th scope="row">${resultBDB.deliveryMethodName}</th>
      <td>${resultBDB.deliveryMethodPrice}円</td>
    </tr>
  </tbody>
</table>

<div class="col-xs-4">
<a href="userList">戻る</a>
</div>


 </div>

</body>
</html>