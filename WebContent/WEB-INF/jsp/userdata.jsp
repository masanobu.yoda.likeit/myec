<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー情報</title>
<link rel="stylesheet" href="css/original/common.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

<jsp:include page="/baselayout/header.jsp" />

<br>
<br>

<div class="container">

<form action="userdata" method="post" class="form-horizonical">

<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
</c:if>

<div class="search-form-area">

  <div class="text-center"><h5>ユーザー情報</h5></div>

  <br>

  <div class="form-group row">
    <label for="loginId" class="col-sm-2 col-form-label">ログインID</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="loginId" name="loginId" value="${userInfo.loginId}">
    </div>
  </div>

  <div class="form-group row">
    <label for="InputPassword" class="col-sm-2 col-form-label">パスワード</label>
    <div class="col-sm-10">
    <input type="password" class="form-control" id="inputPassword" name="password" >
    </div>
  </div>

  <div class="form-group row">
    <label for="InputPassword" class="col-sm-2 col-form-label">パスワード(確認)</label>
    <div class="col-sm-10">
    <input type="password" class="form-control" id="inputPassword" name="passwordConf">
    </div>
  </div>

  <div class="form-group row">
    <label for="userName" class="col-sm-2 col-form-label">ユーザー名</label>
    <div class="col-sm-10">
    <input type="text" class="form-control" id="userName" name="name" value="${userInfo.name}">
    </div>
  </div>

  <div class="form-group row">
    <label for="birthdate" class="col-sm-2 col-form-label">生年月日</label>
    <div class="col-sm-10">
    <input type="date" class="form-control" id="birthdate" name="birthdate" value="${userInfo.birthDate}">
    </div>
  </div>

  <div class="text-center">
   <input type="hidden" name="id" value="${userInfo.id}">
   <button type="submit" value="検索" class="btn btn-primary" name="confirm_button" >更新</button>
  </div>

</div>
</form>

<br>
<br>
<br>
<br>

<table class="table table-striped ">
  <thead class="thead-dark">
    <tr>
      <th scope="col">購入日時</th>
      <th scope="col">配送方法</th>
      <th scope="col">購入金額</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
    <c:forEach var="bd" items="${bdList}">
    <tr>
      <th scope="row">${bd.formatDate}</th>
      <td>${bd.deliveryMethodName}</td>
      <td>${bd.formatTotalPrice}円</td>
      <td>
         <a class="btn btn-primary" href="userbuyhistorydetail?buy_id=${bd.id}">詳細</a>
         <a class="btn btn-danger" href="userbuyhistorydelete?buy_id=${bd.id}">削除</a>
      </td>
    </tr>
    </c:forEach>
  </tbody>
</table>

</div>

</body>
</html>