<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー情報/更新確認</title>
<link rel="stylesheet" href="css/original/common.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

<jsp:include page="/baselayout/header.jsp" />

 <div class="container">

<div class="form-group row alert alert-danger" role="alert">
    <div class="col-sm-10">
    <p class="center-align">下記内容で更新してよろしいでしょうか？</p>
    </div>
  </div>

<form method="post" action="userdataupdateresult">

<div class="text-center">
 <h4>入力内容確認</h4>
</div>

<br>

<div class="form-group row">
 <label for="loginId" class="col-sm-2 col-form-label">ログインID</label>
 <div class="col-sm-10">
 <p class="form-control-plaintext">id0001</p>
 </div>
</div>

<div class="form-group row">
 <label for="userName" class="col-sm-2 col-form-label">ユーザー名</label>
 <div class="col-sm-10">
 <p class="form-control-plaintext">田中太郎</p>
 </div>
</div>

<div class="form-group row">
 <label for="birthDate" class="col-sm-2 col-form-label">生年月日</label>
 <div class="col-sm-10">
 <p class="form-control-plaintext">2019年9月17日</p>
 </div>
</div>

<div class="form-group row">
 <label for="createDate" class="col-sm-2 col-form-label">新規登録日時</label>
 <div class="col-sm-10">
 <p class="form-control-plaintext">2019年9月17日</p>
 </div>
</div>

<div class="form-group row">
 <label for="updateDate" class="col-sm-2 col-form-label">更新日時</label>
 <div class="col-sm-10">
 <p class="form-control-plaintext">2019年9月17日</p>
 </div>
</div>

<div class="form-group row">
   <div class="col-sm-6">
   <a href="userdata" class="btn btn-primary btn-lg btn-block">戻る</a>
   </div>
   <div class="col-sm-6">
   <button type="submit" value="regist" class="btn btn-primary btn-lg btn-block" name="confirm_button">更新</button>
   </div>
  </div>


</form>

</div>

</body>
</html>