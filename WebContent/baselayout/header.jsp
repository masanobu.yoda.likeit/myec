<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<nav class="navbar navbar-dark bg-dark">

  <ul class="nav">
   <li class="nav-item">
   <a href="index" class="btn btn-success">書籍販売サイトTOP</a>
   </li>
  </ul>

  <ul class="nav justify-content-end">
  <li>
    <a href="taikai " class="nav-link text-light">退会</a>
  </li>
  <li>
    <a href="userdata " class="nav-link text-light">ユーザーデータ</a>
  </li>
  <li>
    <a href="cart" class="nav-link text-light">カート</a>
  </li>

  <li>
    <a href="logout" class="btn btn-primary">ログアウト</a>
  </li>
  </ul>

</nav>

<br>