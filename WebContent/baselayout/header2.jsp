<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<header>
 <nav class="navbar navbar-dark bg-dark">
  <ul class="nav ">
  <li class="nav-item">
    <a class="nav-link text-light" >管理者用システム</a>
  </li>
  </ul>
  <ul class="nav justify-content-end">
  <li class="nav-item">
    <a href="index2" class="nav-link text-light">商品一覧</a>
  </li>
  <li class="nav-item">
    <a href="userList" class="nav-link text-light">ユーザー一覧</a>
  </li>
  <li class="nav-item">
    <a href="administratorList" class="nav-link text-light">管理者一覧</a>
  </li>
  <li class="nav-item">
    <a href="login2" class="btn btn-primary">ログアウト</a>
  </li>
  </ul>
 </nav>
</header>
