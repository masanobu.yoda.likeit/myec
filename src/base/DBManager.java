package base;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBManager  {
	private static String url = "jdbc:mysql://localhost:3306/myec_db?useUnicode=true&characterEncoding=utf8&useSSL=false";
	private static String user = "root";
	private static String pass = "password";

	public static Connection getConnection() {
		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection(url,user,pass);
			System.out.println("DBConnected!!");
			return con;
		}catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return con;
	}
}
