package beans;

import java.io.Serializable;
import java.sql.Date;

/**
 *administratorテーブルのデータを格納するためのBeans
 */

public class AdministratorDataBeans implements Serializable {
	private String name;
	private Date birthDate;
	private String loginId;
	private String password;
	private Date createDate;
	private Date updateDate;
	private int id;

	public AdministratorDataBeans() {

	}

	//すべてのデータをセットするコンストラクタ
	public AdministratorDataBeans(int id2, String loginId2, String name2, Date birthDate2, String password2,
			Date createDate2, Date updateDate2) {
		this.id = id2;
		this.loginId = loginId2;
		this.name = name2;
		this.birthDate = birthDate2;
		this.password = password2;
		this.createDate = createDate2;
		this.updateDate = updateDate2;
	}


	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	//検索機能
	public AdministratorDataBeans(String loginId3, String name3, Date birthdate3) {
		this.loginId = loginId3;
        this.name= name3;
        this.birthDate= birthdate3;
	}

	public AdministratorDataBeans(String loginIdData, String nameData) {
		this.loginId = loginIdData;
		this.name = nameData;
	}

	//新規登録
	public AdministratorDataBeans(String loginId) {
		this.loginId = loginId;
	}
}
