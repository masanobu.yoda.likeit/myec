package beans;

import java.io.Serializable;


/**
 * アイテム
 */

public class ItemDataBeans implements Serializable {
	private int id;
	private String name;
	private String detail;
	private int price;
	private String fileName;

    //すべてのデータをセットするコンストラクタ
	public ItemDataBeans(int id,String name,String detail,int price,String file_name) {
		this.id = id;
		this.name = name;
		this.detail = detail;
		this.price = price;
		this.fileName = file_name;
	}

	public ItemDataBeans() {

	}



	public int getId() {
		return id;
	}
	public void setId(int itemId) {
		this.id = itemId;
	}
	public String getName() {
		return name;
	}
	public void setName(String itemName) {
		this.name = itemName;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int itemPrice) {
		this.price = itemPrice;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFormatPrice() {
		return String.format("%,d", this.price);
	}

    //検索機能
	public ItemDataBeans(int id,String name,int price,String detail) {
		this.id = id;
		this.name = name;
		this.price = price;
		this.detail = detail;

	}


}
