package beans;

import java.io.Serializable;
import java.sql.Date;

/**
 * Userテーブルのデータを格納するためのBeans
 */

public class UserDataBeans implements Serializable{
	private String name;
	private Date birthDate;
	private String loginId;
	private String password;
	private Date createDate;
	private Date updateDate;
	private int id;

	//コンストラクタ
	public UserDataBeans() {

	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}





	 //ログインセッションを保存するためのコンストラクタ
	public UserDataBeans(String loginId,String name) {
		this.loginId = loginId;
		this.name = name;
	}


	// 新規登録
	 public UserDataBeans(String loginId) {
		this.loginId = loginId;
	}


	// すべてのデータをセットするコンストラクタ
	 public UserDataBeans(int id,String name, Date birthDate,String loginId,String password,Date createDate,Date updateDate) {
	    	this.id = id;
	    	this.name = name;
	    	this.birthDate = birthDate;
	    	this.loginId = loginId;
	    	this.password = password;
	    	this.createDate = createDate;
	    	this.updateDate = updateDate;
	    }

	//検索機能
	public UserDataBeans(String loginId2, String name2, Date birthdate2) {
		this.loginId = loginId2;
		this.name = name2;
		this.birthDate = birthdate2;
	}

}

