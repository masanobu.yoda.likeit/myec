package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.AdministratorDataBeans;

/**
 * Servlet implementation class AdministratorDAO
 */

public class AdministratorDAO  {

	/*
	 *ログインIDとパスワードに紐づくユーザー情報を返す
	 */
	public AdministratorDataBeans findByLoginInfo(String loginId, String password) {
		Connection con = null;
		try {
			//データベースへ接続
			con = DBManager.getConnection();

			//SELECT文を準備
			String sql ="SELECT * FROM t_administrator WHERE login_id=? and login_password=?";

			//パスワードを暗号化
			String Result = Password.pw(password);

			//SELECT文を実行し、結果表を取得
            PreparedStatement ps =con.prepareStatement(sql);
            ps.setString(1, loginId);
            ps.setString(2, Result);
            ResultSet rs = ps.executeQuery();

            //ログイン失敗時の処理
            if(!rs.next()) {
                return null;
            }

            //ログイン成功時の処理
            String loginIdData = rs.getString("login_id");
            String nameData = rs.getString("name");
            return new AdministratorDataBeans(loginIdData,nameData);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			//データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return null;


	}


	/*
	 * 管理者一覧画面用
	 */
    public List<AdministratorDataBeans> findAll(){
    	Connection conn = null;
    	List<AdministratorDataBeans> aList = new ArrayList<AdministratorDataBeans>();

    	try {
    		conn = DBManager.getConnection();

    		String sql = "SELECT * FROM t_administrator ";

    		Statement stmt = conn.createStatement();
    		ResultSet rs = stmt.executeQuery(sql);

    		while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("login_password");
				Date createDate = rs.getDate("create_date");
				Date updateDate = rs.getDate("update_date");

				AdministratorDataBeans ad = new AdministratorDataBeans(id, loginId, name, birthDate, password, createDate, updateDate);

				aList.add(ad);
    	}
    } catch (SQLException e) {
		e.printStackTrace();
		return null;
	} finally {
		// データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
	}
	return aList;


}

    /*
     * 検索機能用
     */
    public List<AdministratorDataBeans> search(String login_id,String name,String birthdate,String birthdate2){
		Connection conn = null;
		List<AdministratorDataBeans> alist = new ArrayList<AdministratorDataBeans>();
		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM t_administrator WHERE login_id !=''";

			if (!(login_id.isEmpty())) {
				sql += " AND login_id = '" + login_id + "'";
			}

			if (!(name.isEmpty())) {
				sql += " AND name LIKE '%" + name + "%'";
			}
			if (!(birthdate.isEmpty())) {
				sql += " AND birth_date > '" + birthdate + "'";
			}
			if (!(birthdate2.isEmpty())) {
				sql += " AND birth_date < '" + birthdate2 + "'";
			}

		    Statement st = conn.createStatement();
		    ResultSet rs = st.executeQuery(sql);

		    while(rs.next()) {
		    	String LoginId = rs.getString("login_id");
		    	String Name = rs.getString("name");
		    	Date Birthdate = rs.getDate("birth_date");

		    	AdministratorDataBeans ad = new AdministratorDataBeans(LoginId ,Name,Birthdate );
		    	alist.add(ad);

    }
		return alist;

	}catch (SQLException e) {
		e.printStackTrace();
	} finally {
		//データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
	}
	return null;

    }

    /*
     * 管理者情報詳細画面
     */
    public AdministratorDataBeans findById(String targetId) {
    	Connection conn = null;
        try {
        	//DBへ接続
        	conn = DBManager.getConnection();

        	//SELECT文を実行
        	String sql = "SELECT * FROM t_administrator WHERE id = ?";

        	//SELECTを実行し、結果表を取得
        	PreparedStatement ps = conn.prepareStatement(sql);
        	ps.setString(1, targetId);
        	ResultSet rs = ps.executeQuery();

        	//主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
            if(!rs.next()) {
            	return null;
            }

            AdministratorDataBeans ad = new AdministratorDataBeans();

            ad.setId(rs.getInt("id"));
            ad.setLoginId(rs.getString("login_id"));
            ad.setName(rs.getString("name"));
            ad.setBirthDate(rs.getDate("birth_date"));
            ad.setCreateDate(rs.getDate("create_date"));
            ad.setUpdateDate(rs.getDate("update_date"));

            return ad;


        } catch (SQLException e) {
			e.printStackTrace();
		} finally {
			//データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return null;

    }

    /*
     * 削除画面
     */
    public AdministratorDataBeans findById2(String targetId) {
    	Connection conn = null;
    	try {
    		//DBへ接続
    		conn = DBManager.getConnection();

    		//SELECT文を準備
    		String sql = "SELECT * FROM t_administrator WHERE id = ?";

            //SELECTを実行し、結果表を取得
    		PreparedStatement ps = conn.prepareStatement(sql);
    		ps.setString(1, targetId);
    		ResultSet rs = ps.executeQuery();

    		//主キーに紐づくレコードは1件のみなので、rs.next()は一回だけ行う
    		if(!rs.next()) {
    			return null;
    		}

    		AdministratorDataBeans adb = new AdministratorDataBeans();

    		adb.setId(rs.getInt("id"));
    		adb.setLoginId(rs.getString("login_id"));
    		adb.setName(rs.getString("name"));
    		adb.setBirthDate(rs.getDate("birth_date"));
    		adb.setCreateDate(rs.getDate("create_date"));
    		adb.setUpdateDate(rs.getDate("update_date"));
    		adb.setPassword(rs.getString("login_password"));
    		return adb;

    	} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			//データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return null;

    }

    public AdministratorDataBeans delete(String targetId) {
    	Connection conn = null;
    	try {
    		//データベースへ接続
    		conn = DBManager.getConnection();

    		//DELETE文を準備
    		String sql = "DELETE FROM t_administrator WHERE id = ?";

    		//DELETEを実行
    		PreparedStatement ps = conn.prepareStatement(sql);
    		ps.setString(1, targetId);

    		int result = ps.executeUpdate();

    		System.out.println(result);

    		ps.close();


    	} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			//データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return null;
    }


    /*
     * 管理者情報更新画面
     */
    public void update(String password,String name,String birthdate,String targetId) {
    	Connection conn = null;

		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			// UPDATE文を準備
			String sql = "UPDATE t_administrator SET name=?,birth_date=?,update_date=now()";

			int count = 3;
			if (!(password.isEmpty())) {
				sql += ",login_password=?";
				count+=1;
			}

			sql += " WHERE id = ?";

			//パスワードを暗号化
			String Result = Password.pw(password);

			// UPDATE文を実行
			PreparedStatement pSmt = conn.prepareStatement(sql);
			pSmt.setString(1, name);
			pSmt.setString(2, birthdate);
			if (!(password.isEmpty())) {
				pSmt.setString(3, Result);
			}
			pSmt.setString(count, targetId);
			int result = pSmt.executeUpdate();

			System.out.println(result);

			pSmt.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			//データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

    }

    /*
     * 管理者新規登録用
     */
    public void insert(String loginId,String password,String name,String birthdate) {
    	Connection conn = null;
    	try {
    		//DBへ接続
    		conn = DBManager.getConnection();

    		//INSERT文を準備
    		String sql = "INSERT INTO t_administrator(login_id,login_password,name,birth_date,create_date,update_date) VALUES(?,?,?,?,now(),now())";

    	    //パスワードを暗号化
    		String Result = Password.pw(password);

    		//INSERT文を実行
    		PreparedStatement ps = conn.prepareStatement(sql);
    		ps.setString(1, loginId);
    		ps.setString(2, Result);
    		ps.setString(3, name);
    		ps.setString(4, birthdate);
    		int result = ps.executeUpdate();

    		System.out.println(result);

    		ps.close();

    	} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			//データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
    }



    /*
     * 更新・新規登録時、重複防止
     */
    public AdministratorDataBeans findByLoginId(String loginId) {
		Connection con = null;
		try {
			//データベースへ接続
			con = DBManager.getConnection();

			//SELECT文を準備
			String sql = "SELECT * FROM t_Administrator WHERE login_id=?";

			//SELECTを実行し、結果表を取得
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, loginId);
			ResultSet rs = ps.executeQuery();

			//ログイン失敗時の処理
			if(!rs.next()) {
				return null;
			}

			//ログイン成功時の処理
			String loginIdData = rs.getString("login_id");
			return new AdministratorDataBeans(loginIdData);

		}catch (SQLException e) {
			e.printStackTrace();
		} finally {
			//データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return null;


	}

}
