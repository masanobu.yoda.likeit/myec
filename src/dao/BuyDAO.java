package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;

import base.DBManager;
import beans.BuyDataBeans;

/**
 * Servlet implementation class BuyDAO
 */

public class BuyDAO  {
	/*
	 * 購入IDによる購入情報検索
	 */
	public ArrayList<BuyDataBeans> findAll(int userId) throws SQLException{
		Connection con = null;
		ArrayList<BuyDataBeans> bdList = new ArrayList<BuyDataBeans>();

		try {
			//データベースへ接続
			con = DBManager.getConnection();

			//SELECT文を準備
			String sql = "SELECT * FROM t_buy" + " JOIN m_delivery_method" + " ON t_buy.delivery_method_id = m_delivery_method.id" + " WHERE t_buy.user_id = ?";

			//SELECT文を実行し、結果表を取得
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, userId);
			ResultSet rs = ps.executeQuery();


			//結果表に格納されたレコードの内容を
			//BuyDataBeansインスタンスに設定し、AllayListインスタンスに追加
			while(rs.next()) {
				BuyDataBeans bd = new BuyDataBeans();
				bd.setId(rs.getInt("id"));
				bd.setTotalPrice(rs.getInt("total_price"));
				bd.setBuyDate(rs.getTimestamp("create_date"));
				bd.setDeliveryMethodId(rs.getInt("delivery_method_id"));
				bd.setUserId(rs.getInt("user_id"));
				bd.setDeliveryMethodPrice(rs.getInt("price"));
				bd.setDeliveryMethodName(rs.getString("name"));

				bdList.add(bd);
			}
          return bdList;

		}catch (SQLException e) {
			e.printStackTrace();
		} finally {
			//データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return null;
	}


	/*
	 * 購入情報登録処理
	 */
	public static int insertBuy(BuyDataBeans bdb) throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;
		int autoIncKey = -1;
		try {
			conn = DBManager.getConnection();

		    ps = conn.prepareStatement("INSERT INTO t_buy (user_id,total_price,delivery_method_id,create_date) VALUES(?,?,?,?)",Statement.RETURN_GENERATED_KEYS);
		    ps.setInt(1, bdb.getUserId());
		    ps.setInt(2, bdb.getTotalPrice());
            ps.setInt(3, bdb.getDeliveryMethodId());
            ps.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
            ps.executeUpdate();

            ResultSet rs = ps.getGeneratedKeys();
            if(rs.next()) {
            	autoIncKey = rs.getInt(1);
            }

            return autoIncKey;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (conn != null) {
				conn.close();
			}
		}

	}



	/*
	 * 購入IDによる購入情報検索
	 */
	public static BuyDataBeans getBuyDataBeansById(int buyId) throws SQLException{
		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM t_buy JOIN m_delivery_method ON t_buy.delivery_method_id = m_delivery_method.id WHERE t_buy.id=?";

		    PreparedStatement ps = conn.prepareStatement(sql);
		    ps.setInt(1, buyId);
		    ResultSet rs = ps.executeQuery();

		    BuyDataBeans bdb = new BuyDataBeans();
		    if(rs.next()) {
		    	bdb.setId(rs.getInt("id"));
		    	bdb.setTotalPrice(rs.getInt("total_price"));
		    	bdb.setBuyDate(rs.getTimestamp("create_date"));
		    	bdb.setDeliveryMethodId(rs.getInt("delivery_method_id"));
		    	bdb.setUserId(rs.getInt("user_id"));
		    	bdb.setDeliveryMethodPrice(rs.getInt("price"));
		    	bdb.setDeliveryMethodName(rs.getString("name"));
		    }

            return bdb;

		}catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
	}



	public static BuyDataBeans getBuyDataBeansDelete(int buyId) throws SQLException{
		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "DELETE FROM t_buy JOIN m_delivery_method ON t_buy.delivery_method_id = m_delivery_method.id WHERE t_buy.id=?";

		    PreparedStatement ps = conn.prepareStatement(sql);
		    ps.setInt(1, buyId);

		    int result = ps.executeUpdate();

		    System.out.println(result);

		    ps.close();

		}catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
		return null;
	}

}
