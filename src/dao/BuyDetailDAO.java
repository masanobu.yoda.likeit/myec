package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.annotation.WebServlet;

import base.DBManager;
import beans.BuyDetailDataBeans;
import beans.ItemDataBeans;

/**
 * Servlet implementation class BuyDetailDAO
 */
@WebServlet("/BuyDetailDAO")
public class BuyDetailDAO {


	/*
	 * 購入詳細登録処理
	 */
	public static void insertBuyDetail(BuyDetailDataBeans bddb)throws SQLException {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "INSERT INTO t_buy_detail(buy_id,item_id) VALUES (?,?)";

		    PreparedStatement ps = conn.prepareStatement(sql);
		    ps.setInt(1, bddb.getBuyId());
		    ps.setInt(2, bddb.getItemId());
		    int result = ps.executeUpdate();

		    System.out.println(result);

		    ps.close();

		}catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (conn != null) {
				conn.close();
			}
		}

	}

		/*
		 * 購入IDによる購入詳細情報検索
		 */
		public static ArrayList<ItemDataBeans> getItemDataBeansListByBuyId(int buyId) throws SQLException{
            Connection conn = null;
            try {
            	conn =  DBManager.getConnection();

            	String sql = "SELECT m_item.id,m_item.name,m_item.price FROM t_buy_detail JOIN m_item ON t_buy_detail.item_id = m_item.id WHERE t_buy_detail.buy_id = ?";

                PreparedStatement ps = conn.prepareStatement(sql);
                ps.setInt(1, buyId);
                ResultSet rs = ps.executeQuery();

                ArrayList<ItemDataBeans> buyDetailItemList = new ArrayList<ItemDataBeans>();

                while(rs.next()) {
                	ItemDataBeans idb = new ItemDataBeans();
                	idb.setId(rs.getInt("id"));
                	idb.setName(rs.getString("name"));
                	idb.setPrice(rs.getInt("price"));

                	buyDetailItemList.add(idb);
                }

                return buyDetailItemList;

            }catch (SQLException e) {
    			System.out.println(e.getMessage());
    			throw new SQLException(e);
    		} finally {
    			if (conn != null) {
    				conn.close();
    			}
    		}
    	}




		public static BuyDetailDataBeans getItemDataBeansListDelete(int buyId) throws SQLException{
            Connection conn = null;
            try {
            	conn =  DBManager.getConnection();

            	String sql = "DELETE FROM t_buy_detail JOIN m_item ON t_buy_detail.item_id = m_item.id WHERE t_buy_detail.buy_id = ?";

                PreparedStatement ps = conn.prepareStatement(sql);
                ps.setInt(1, buyId);

                int result = ps.executeUpdate();

                System.out.println(result);

                ps.close();

            }catch (SQLException e) {
    			System.out.println(e.getMessage());
    			throw new SQLException(e);
    		} finally {
    			if (conn != null) {
    				conn.close();
    			}
    		}
			return null;
    	}
	}



