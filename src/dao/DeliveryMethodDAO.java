package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.annotation.WebServlet;

import base.DBManager;
import beans.DeliveryMethodDataBeans;

/**
 * Servlet implementation class DeliveryMethodDAO
 */
@WebServlet("/DeliveryMethodDAO")
public class DeliveryMethodDAO {


	/*
	 * DBに登録されている配送方法を取得
	 */
	public static ArrayList<DeliveryMethodDataBeans> getAllDeliveryMethodDataBeans() throws SQLException{
		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM m_delivery_method";

			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();

			ArrayList<DeliveryMethodDataBeans> deliveryMethodDataBensList = new ArrayList<DeliveryMethodDataBeans>();
			while(rs.next()) {
				DeliveryMethodDataBeans dmdb = new DeliveryMethodDataBeans();
				dmdb.setId(rs.getInt("id"));
				dmdb.setName(rs.getString("name"));
				dmdb.setPrice(rs.getInt("price"));
				deliveryMethodDataBensList.add(dmdb);
			}
			return deliveryMethodDataBensList;

		}catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
	}



	/*
	 * 配送方法をIDをもとに取得
	 */
	public static DeliveryMethodDataBeans getDeliveryMethodDataBeansByID(int DeliveryMethodId) throws SQLException{
       Connection conn = null;
       try {
    	   conn = DBManager.getConnection();

    	   String sql = "SELECT * FROM m_delivery_method WHERE id=?";

           PreparedStatement ps = conn.prepareStatement(sql);
           ps.setInt(1, DeliveryMethodId);
           ResultSet rs = ps.executeQuery();

           DeliveryMethodDataBeans dmdb = new DeliveryMethodDataBeans();
           if(rs.next()) {
        	   dmdb.setId(rs.getInt("id"));
        	   dmdb.setName(rs.getString("name"));
        	   dmdb.setPrice(rs.getInt("price"));
           }

           return dmdb;

       }catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
	}
}
