package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.ItemDataBeans;

/*
 * アイテムテーブル用のDAO
 */
public class ItemDAO {

	/*
	 * ランダムで引数指定分のItemDataBeansを取得
	 */
	public static ArrayList<ItemDataBeans> getRandItem(int limit)throws SQLException{
		Connection conn = null;
		 ArrayList<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();
		try {
			//DBへ接続
			conn = DBManager.getConnection();

			//SELECT文を準備
			String sql = "SELECT * FROM m_item ORDER BY RAND() LIMIT ?";

			//SELECT文を実行して、結果表を取得
			PreparedStatement ps = conn.prepareStatement(sql);
		    ps.setInt(1, limit);
		    ResultSet rs = ps.executeQuery();

		    //結果表に格納されたレコードの内容を
            //ItemDataBeansインスタンスに設定し、AllayListインスタンスに追加
		    while(rs.next()) {
		    	ItemDataBeans item = new ItemDataBeans();
		    	item.setId(rs.getInt("id"));
		    	item.setName(rs.getString("name"));
		    	item.setDetail(rs.getString("detail"));
		    	item.setPrice(rs.getInt("price"));
		    	item.setFileName(rs.getString("file_name"));
		    	itemList.add(item);
		    }
		}catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return itemList;
	}


	/*
	 * 商品IDによる商品検索
	 */
	public static ItemDataBeans getItemByItemID(int itemId) {
		Connection conn = null;
		try {
			//DBへ接続
			conn = DBManager.getConnection();

			//SELECT文を準備
			String sql = "SELECT * FROM m_item WHERE id = ?";

			//SELECT文を実行して、結果表を取得
			PreparedStatement ps = conn.prepareStatement(sql);
	        ps.setInt(1, itemId);
	        ResultSet rs = ps.executeQuery();

	        //主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
    		if(!rs.next()) {
    			return null;
    		}

    		ItemDataBeans DB = new ItemDataBeans();

			DB.setId(rs.getInt("id"));
			DB.setName(rs.getString("name"));
			DB.setDetail(rs.getString("detail"));
			DB.setPrice(rs.getInt("price"));
			DB.setFileName(rs.getString("file_name"));

			return DB;

		}catch (SQLException e) {
			e.printStackTrace();
		} finally {
			//データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return null;
	}


	/*
	 * 商品検索
	 */
	public static ArrayList<ItemDataBeans> getItemByItemName(String searchWord,int pageNum,int pageMaxItemCount){
		Connection conn = null;
		PreparedStatement ps = null;
		try {
            int startiItemNum = (pageNum - 1)*pageMaxItemCount;
            conn = DBManager.getConnection();

			if(searchWord.length() == 0) {
			   //全検索
			   ps = conn.prepareStatement("SELECT * FROM m_item ORDER BY id ASC LIMIT ?,?");
			   ps.setInt(1, startiItemNum);
			   ps.setInt(2, pageMaxItemCount);
			}else {
			   //商品名検索
			   ps = conn.prepareStatement("SELECT * FROM m_item WHERE name LIKE ? ORDER BY id ASC LIMIT ?,?");
			   ps.setString(1, "%" + searchWord + "%");
			   ps.setInt(2, startiItemNum);
			   ps.setInt(3, pageMaxItemCount);
			}

			ResultSet rs = ps.executeQuery();
			ArrayList<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

			while (rs.next()) {
				ItemDataBeans item = new ItemDataBeans();
				item.setId(rs.getInt("id"));
				item.setName(rs.getString("name"));
				item.setDetail(rs.getString("detail"));
				item.setPrice(rs.getInt("price"));
				item.setFileName(rs.getString("file_name"));
				itemList.add(item);

			}

			return itemList;

		}catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

	}


	/*
	 * 商品総数を獲得
	 */
	public static double getItemCount(String searchWord) throws SQLException{
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = DBManager.getConnection();
			ps = conn.prepareStatement("select count(*) as cnt from m_item where name like ?");
            ps.setString(1, "%"+searchWord+"%");
            ResultSet rs = ps.executeQuery();
            double db = 0.0;
            while(rs.next()) {
            	db = Double.parseDouble(rs.getString("cnt"));
            }
            return db;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (conn != null) {
				conn.close();
			}
			}
	}

	/*
	 * アイテム一覧画面用
	 */
	public List<ItemDataBeans> findAll(){
        Connection conn = null;
        List<ItemDataBeans> iList = new ArrayList<ItemDataBeans>();

		try {
			//DBへ接続
			conn = DBManager.getConnection();

			//SELECT文を準備
			String sql = "SELECT * FROM m_item ";

			//SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            //結果表に格納されたレコードの内容を
            //ItemDataBeansインスタンスに設定し、AllayListインスタンスに追加
            while(rs.next()) {
            	int id = rs.getInt("id");
            	String name = rs.getString("name");
            	String detail = rs.getString("detail");
            	int price = rs.getInt("price");
            	String filename = rs.getString("file_name");

                ItemDataBeans ID = new ItemDataBeans(id,name,detail,price,filename);

                iList.add(ID);
            }


		}catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return iList;
	}


	/*
	 * アイテム詳細画面用
	 */
    public ItemDataBeans findById(String targetId) {
    	Connection conn = null;
    	try {
    		//DBへ接続
    		conn = DBManager.getConnection();

    		//SELECT文を準備
    		String sql = "SELECT * FROM m_item WHERE id = ?";

    		//SELECT文を実行し、結果表を取得
    		PreparedStatement ps = conn.prepareStatement(sql);
    		ps.setString(1, targetId);
    		ResultSet rs = ps.executeQuery();

    		//主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
    		if(!rs.next()) {
    			return null;
    		}

    		ItemDataBeans DB = new ItemDataBeans();

			DB.setId(rs.getInt("id"));
			DB.setName(rs.getString("name"));
			DB.setDetail(rs.getString("detail"));
			DB.setPrice(rs.getInt("price"));
			DB.setFileName(rs.getString("file_name"));

			return DB;

    	}catch (SQLException e) {
			e.printStackTrace();
		} finally {
			//データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return null;
    }


    /*
     * アイテムデータ新規登録用
     */
    public void insert(String name,String detail,String price,String filename) {
    	Connection conn = null;

		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			// INSERT文を準備
			String sql = "INSERT INTO m_item(name,detail,price,file_name) VALUES(?,?,?,?)";

			// INSERTを実行
			PreparedStatement pSmt = conn.prepareStatement(sql);
			pSmt.setString(1, name);
			pSmt.setString(2, detail);
			pSmt.setString(3, price);
			pSmt.setString(4, filename);
			int result = pSmt.executeUpdate();

			System.out.println(result);

			pSmt.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			//データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

    }


    /*
     * アイテムデータ削除画面用
     */
    public ItemDataBeans findById2(String targetId) {
    	Connection conn = null;
    	try {
    		//DBへ接続
    		conn = DBManager.getConnection();

    		//SELECT文を準備
    		String sql = "SELECT * FROM m_item WHERE id = ?";

    		//SELECT文を実行し、結果表を取得
    		PreparedStatement ps = conn.prepareStatement(sql);
    		ps.setString(1, targetId);
    		ResultSet rs = ps.executeQuery();

    		//主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
    	    if(!rs.next()) {
    	    	return null;
    	    }

            ItemDataBeans ID = new ItemDataBeans();
            ID.setId(rs.getInt("id"));
            ID.setName(rs.getString("name"));
            ID.setDetail(rs.getString("detail"));
            ID.setPrice(rs.getInt("price"));
            ID.setFileName(rs.getString("file_name"));
            return ID;

    	}catch (SQLException e) {
			e.printStackTrace();
		} finally {
			//データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return null;
    }


    public ItemDataBeans delete(String targetId) {
    	Connection conn = null;
    	try {
    		//DBへ接続
    		conn = DBManager.getConnection();

    		//DELETE文を作成
    		String sql = "DELETE FROM m_item WHERE id = ?";

    		//DELETEを実行
    		PreparedStatement ps = conn.prepareStatement(sql);
    		ps.setString(1, targetId);

    		int result = ps.executeUpdate();

    		System.out.println(result);

    		ps.close();

    	} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			//データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return null;
    }


    /*
     * アイテムデータ更新画面用
     */
    public void update(String name,String price,String detail,String filename,String targetId) {
    	Connection conn = null;
    	try {
    		//データベースへ接続
    		conn = DBManager.getConnection();

    		//UPDATE文を準備
    		String sql = "UPDATE m_item SET name=?,price=?,detail=?,file_name=? WHERE id=?";

    	    //UPDATE文を実行
    		PreparedStatement ps = conn.prepareStatement(sql);
    		ps.setString(1, name);
    		ps.setString(2, price);
    		ps.setString(3, detail);
    		ps.setString(4, filename);
    		ps.setString(5, targetId);

    		int result = ps.executeUpdate();

    		System.out.println(result);

    		ps.close();

    	} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			//データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
    }


    /*
     * 検索機能用
     */
    public List<ItemDataBeans> search(String targetId,String name,String price,String price2,String detail){
        Connection conn = null;
        List<ItemDataBeans> ilist = new ArrayList<ItemDataBeans>();

		try {
			//DBへ接続
			conn = DBManager.getConnection();

			//SELECT文を準備
			String sql = "SELECT * FROM m_item WHERE id !=0";

			//部分一致検索(SELECT文を準備）

			if (!(targetId.isEmpty())) {
				sql += " AND id = " + targetId + "";
			}

            if(!(name.isEmpty())) {
            	sql += " AND name LIKE '%" + name + "%'";
            }

            if(!(price.isEmpty())) {
            	sql += " AND price > " + price + "";
            }

            if(!(price2.isEmpty())) {
            	sql += " AND price < " + price2 + "";
            }

            if(!(detail.isEmpty())) {
            	sql += " AND detail LIKE '%" + detail + "%'";
            }

			//SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            //結果表に格納されたレコードの内容を
            //ItemDataBeansインスタンスに設定し、AllayListインスタンスに追加
            while(rs.next()) {
            	int id = rs.getInt("id");
            	String Name = rs.getString("name");
            	int Price = rs.getInt("price");
            	String Detail = rs.getString("detail");

                ItemDataBeans ID = new ItemDataBeans(id,Name,Price,Detail);

                ilist.add(ID);
            }

         return ilist;

		}catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
        return null;
	}



}
