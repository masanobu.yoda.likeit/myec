package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.UserDataBeans;



public class UserDAO  {


	/*
	 *ログインIDとパスワードに紐づくユーザー情報を返す
	 */
	public UserDataBeans findByLoginInfo(String loginId, String password) {
		Connection con = null;
		try {
			//データベースへ接続
			con = DBManager.getConnection();

			//SELECT文を準備
			String sql ="SELECT * FROM t_user WHERE login_id=? and login_password=?";

			//パスワードを暗号化
			String Result = Password.pw(password);

			//SELECT文を実行し、結果表を取得
            PreparedStatement ps =con.prepareStatement(sql);
            ps.setString(1, loginId);
            ps.setString(2, Result);
            ResultSet rs = ps.executeQuery();

            //ログイン失敗時の処理
            if(!rs.next()) {
                return null;
            }

            //ログイン成功時の処理
            int idData = rs.getInt("id");
            String nameData = rs.getString("name");
            Date birthdateData = rs.getDate("birth_date");
            String loginIdData = rs.getString("login_id");
            String passwordData = rs.getString("login_password");
            Date createdateData = rs.getDate("create_date");
            Date updatedateData = rs.getDate("update_date");

            return new UserDataBeans(idData,nameData,birthdateData,loginIdData, passwordData,createdateData,updatedateData);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			//データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return null;


	}


	/*
	 *ユーザー新規登録用
	 */
	public void insert(String loginId, String password, String name, String birthdate) {
		Connection con = null;

		try {
			//データベースへ接続
			con = DBManager.getConnection();

			//INSERT文を準備
			String sql = "INSERT INTO t_user(login_id,login_password,name,birth_date,create_date,update_date) VALUES(?,?,?,?,now(),now())";

		    //パスワードを暗号化
		    String Result = Password.pw(password);

			//INSERTを実行
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, loginId);
			ps.setString(2, Result);
			ps.setString(3, name);
			ps.setString(4, birthdate);
			int result =  ps.executeUpdate();

			System.out.println(result);

			ps.close();
		}catch (SQLException e) {
			e.printStackTrace();
		} finally {
			//データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

	}


	/*
	 * 更新・新規登録時、重複防止
	 */
	public UserDataBeans findByLoginId(String loginId) {
		Connection con = null;
		try {
			//データベースへ接続
			con = DBManager.getConnection();

			//SELECT文を準備
			String sql = "SELECT * FROM t_user WHERE login_id=?";

			//SELECTを実行し、結果表を取得
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, loginId);
			ResultSet rs = ps.executeQuery();

			//ログイン失敗時の処理
			if(!rs.next()) {
				return null;
			}

			//ログイン成功時の処理
			String loginIdData = rs.getString("login_id");
			return new UserDataBeans(loginIdData);

		}catch (SQLException e) {
			e.printStackTrace();
		} finally {
			//データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return null;


	}

	/*
	 * 詳細画面用
	 */
	public UserDataBeans findById(String targetId) {
		Connection conn = null;
		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//SELECT文を準備
			String sql = "SELECT * FROM t_user WHERE id = ?";

			//SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, targetId);
			ResultSet rs = pStmt.executeQuery();

			//主キーに紐づくレコードは1件のみなので、rs.next()は一回だけ行う

			if (!rs.next()) {
				return null;
			}

			UserDataBeans user = new UserDataBeans();

			user.setId(rs.getInt("id"));
			user.setLoginId(rs.getString("login_id"));
			user.setName(rs.getString("name"));
			user.setBirthDate(rs.getDate("birth_date"));
			user.setCreateDate(rs.getDate("create_date"));
			user.setUpdateDate(rs.getDate("update_date"));

			return user;

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			//データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return null;

	}


	/*
	 * すべてのユーザー情報を取得する ユーザー一覧画面用
	 */
	public List<UserDataBeans> findAll(){
		Connection conn = null;
		List<UserDataBeans> userList = new ArrayList<UserDataBeans>();

		try {
	        //DBへ接続
			conn = DBManager.getConnection();

			//SELECT文を準備
			String sql = "SELECT * FROM t_user";

			//SELECT文を実行し、結果表を取得
			Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            //結果表に格納されたレコードの内容を
            //UserDataBeansインスタンスに設定し、AllayListインスタンスに追加
            while(rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                Date birthdate = rs.getDate("birth_date");
                String loginId = rs.getString("login_id");
                String password = rs.getString("login_password");
                Date createdate = rs.getDate("create_date");
                Date updatedate = rs.getDate("update_date");

                UserDataBeans user = new UserDataBeans(id,name,birthdate,loginId,password,createdate,updatedate);

                userList.add(user);

            }
		}catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}


	/*
	 * 検索機能用
	 */
	public List<UserDataBeans> search(String login_id,String name,String birthdate,String birthdate2){
		Connection conn = null;
		List<UserDataBeans> userlist = new ArrayList<UserDataBeans>();
		try {
			//DBへ接続
			conn = DBManager.getConnection();

			//SELECT文を準備
			String sql = "SELECT * FROM t_user WHERE login_id != ''";

			//部分一致
			if (!(login_id.isEmpty())) {
				sql += " AND login_id = '" + login_id + "'";
			}

			if (!(name.isEmpty())) {
				sql += " AND name LIKE '%" + name + "%'";
			}
			if (!(birthdate.isEmpty())) {
				sql += " AND birth_date > '" + birthdate + "'";
			}
			if (!(birthdate2.isEmpty())) {
				sql += " AND birth_date < '" + birthdate2 + "'";
			}


			//SELECT文を実行し、結果表を取得
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(sql);

			//結果表に格納されたレコードの内容を
			//UserDataBeansインスタンスに設定し、AllayListインスタンスに追加
			while(rs.next()){
				String LoginId = rs.getString("login_id");
				String Name = rs.getString("name");
				Date Birthdate = rs.getDate("birth_date");

				UserDataBeans user = new UserDataBeans(LoginId,Name,Birthdate);
				userlist.add(user);
			}

			return userlist;

		}catch (SQLException e) {
			e.printStackTrace();
		} finally {
			//データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return null;
	}



    /*
     * 更新機能
     */
	public void update(String password, String name, String birthdate, String loginId,String targetId) {
		Connection conn = null;

		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			// UPDATE文を準備
			String sql = "UPDATE t_user SET name=?,birth_date=?,update_date=now(),login_id=?";

			int count = 4;
			if (!(password.isEmpty())) {
				sql += ",login_password=?";
				count+=1;
			}

			sql += " WHERE id = ?";

			//パスワードを暗号化
			String Result = Password.pw(password);

			// UPDATE文を実行
			PreparedStatement pSmt = conn.prepareStatement(sql);
			pSmt.setString(1, name);
			pSmt.setString(2, birthdate);
			pSmt.setString(3, loginId);
			if (!(password.isEmpty())) {
				pSmt.setString(4, Result);
			}
			pSmt.setString(count, targetId);
			int result = pSmt.executeUpdate();

			System.out.println(result);

			pSmt.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			//データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

	}




	/*
	 * 退会機能
	 */
	public UserDataBeans delete(String targetId) {
		Connection conn = null;

		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			// DELETE文を準備
			String sql = "DELETE FROM t_user WHERE id = ?";

			// DELETEを実行
			PreparedStatement pSmt = conn.prepareStatement(sql);
			pSmt.setString(1, targetId);

			int result = pSmt.executeUpdate();

			System.out.println(result);

			pSmt.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			//データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return null;

	}

	}













