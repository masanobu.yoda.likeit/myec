package ec;

import java.util.ArrayList;

import javax.servlet.annotation.WebServlet;

import beans.ItemDataBeans;

/**
 * Servlet implementation class EChelper
 */
@WebServlet("/EChelper")
public class EChelper {
	//ログイン
	static final String ADMINISTRATORDETAIL_PAGE = "/WEB-INF/jsp/administratorDetail.jsp";

	static final String ADMINISTRATORDELETE_PAGE = "/WEB-INF/jsp/administratorDelete.jsp";

	static final String ADMINISTRATORLIST_PAGE = "/WEB-INF/jsp/administratorList.jsp";

	static final String ADMINISTRATORCREATE_PAGE = "/WEB-INF/jsp/administratorCreate.jsp";

	static final String ADMINISTRATORUPDATE_PAGE = "/WEB-INF/jsp/administratorUpdate.jsp";

	static final String BUY_PAGE = "/WEB-INF/jsp/buy.jsp";

	static final String BUY_CONFIRM_PAGE = "/WEB-INF/jsp/buyconfirm.jsp";

	static final String BUY_RESULT_PAGE = "/WEB-INF/jsp/buyresult.jsp";

	static final String LOGIN_PAGE = "/WEB-INF/jsp/login.jsp";

	static final String LOGIN_PAGE2 = "/WEB-INF/jsp/login2.jsp";

    static final String INDEX_PAGE = "/WEB-INF/jsp/index.jsp";

    static final String INDEX_PAGE2 = "/WEB-INF/jsp/index2.jsp";

    static final String ITEM_PAGE = "/WEB-INF/jsp/item.jsp";

    static final String ITEMDATACREATE_PAGE = "/WEB-INF/jsp/itemdatacreate.jsp";

    static final String ITEMDATADETAIL_PAGE = "/WEB-INF/jsp/itemdatadetail.jsp";

    static final String ITEMDATAUPDATE_PAGE = "/WEB-INF/jsp/itemdataupdate.jsp";

    static final String ITEMDATAUPDELETE_PAGE = "/WEB-INF/jsp/itemdatadelete.jsp";

    static final String ITEM_SEARCH_RESULT_PAGE = "/WEB-INF/jsp/itemsearchresult.jsp";

    static final String LOGOUT_PAGE = "/WEB-INF/jsp/logout.jsp";

    static final String REGIST_PAGE = "/WEB-INF/jsp/regist.jsp";

    static final String REGIST_CONFIRM_PAGE = "/WEB-INF/jsp/registconfirm.jsp";

    static final String REGIST_RESULT_PAGE = "/WEB-INF/jsp/registresult.jsp";

    static final String USERBUYHISTORY_DELETE_PAGE = "/WEB-INF/jsp/userbuyhistorydelete.jsp";

    static final String USERBUYHISTORY_DETAIL_PAGE = "/WEB-INF/jsp/userbuyhistorydetail.jsp";

    static final String USERBUYHISTORY_DETAIL_PAGE2 = "/WEB-INF/jsp/userbuyhistorydetail2.jsp";

    static final String USERDATA_PAGE = "/WEB-INF/jsp/userdata.jsp";

    static final String USERLIST_PAGE = "/WEB-INF/jsp/userList.jsp";

    static final String USERDATA_UPDATE_CONFIRM_PAGE = "/WEB-INF/jsp/userdataupdateconfirm.jsp";

    static final String USERDATA_UPDATE_RESULT_PAGE = "/WEB-INF/jsp/userdataupdateresult.jsp";

    static final String USERDETAIL_PAGE = "/WEB-INF/jsp/userDetail.jsp";

    static final String TAIKAI_PAGE = "/WEB-INF/jsp/taikai.jsp";

	static final String CART_PAGE = "/WEB-INF/jsp/cart.jsp";





	/*
	 * 商品の合計金額を算出
	 */
	public static int getTotalItemPrice(ArrayList<ItemDataBeans> items) {
		int total = 0;
		for (ItemDataBeans item :items) {
			total += item.getPrice();
		}
		return total;
	}




}