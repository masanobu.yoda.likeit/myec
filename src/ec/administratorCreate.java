package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.AdministratorDataBeans;
import dao.AdministratorDAO;

/**
 * Servlet implementation class administratorCreate
 */
@WebServlet("/administratorCreate")
public class administratorCreate extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO ログインセッションがない場合、ログイン画面にリダイレクトさせる
		Object UserInfo;
		HttpSession session = request.getSession();
	    UserInfo=(Object)session.getAttribute("userInfo");
		if(UserInfo==null) {
		  response.sendRedirect("login2");
		  return;
		}


		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher(EChelper.ADMINISTRATORCREATE_PAGE);
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		//リクエストパラメータの入力項目を取得
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String passwordc = request.getParameter("passwordConf");
		String name = request.getParameter("name");
		String birthdate = request.getParameter("birthdate");

		AdministratorDAO Adao = new AdministratorDAO();
		AdministratorDataBeans admin = Adao.findByLoginId(loginId);

		/**登録失敗**/
		if( !(password.equals(passwordc)) || name.equals("") || birthdate.equals("") || loginId.equals("") || admin != null) {
			if(admin != null) {
				//リクエストスコープにエラーメッセージをセット
				request.setAttribute("errMsg", "ログインIDが重複しています");

				request.setAttribute("name", name);
				request.setAttribute("birthdate", birthdate);

				//新規登録jspにフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher(EChelper.ADMINISTRATORCREATE_PAGE);
				dispatcher.forward(request, response);
				return;
			}

			//リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			request.setAttribute("loginId", loginId);
			request.setAttribute("name", name);
			request.setAttribute("birthdate", birthdate);

			//新規登録jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher(EChelper.ADMINISTRATORCREATE_PAGE);
			dispatcher.forward(request, response);
			return;
		}

		/**登録成功**/
		//リクエストパラメータの入力項目を引数に渡して、DAOのメソッドを実行
	    AdministratorDAO adao = new AdministratorDAO();
        adao.insert(loginId, password, name, birthdate);

        //管理者一覧画面のサーブレットにリダイレクト
        response.sendRedirect("administratorList");

	}

}
