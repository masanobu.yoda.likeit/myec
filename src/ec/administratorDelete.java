package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.AdministratorDataBeans;
import dao.AdministratorDAO;

/**
 * Servlet implementation class administratorDelete
 */
@WebServlet("/administratorDelete")
public class administratorDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// TODO ログインセッションがない場合、ログイン画面にリダイレクトさせる
				Object UserInfo;
				HttpSession session = request.getSession();
			    UserInfo=(Object)session.getAttribute("userInfo");
				if(UserInfo==null) {
				  response.sendRedirect("login2");
				  return;
				}



		//URLからGETパラメータとしてidを受け取る
		String id = request.getParameter("id");

		//idを引数にして、idに紐づくユーザー情報を出力する
		AdministratorDAO adao = new AdministratorDAO();
		AdministratorDataBeans ad = adao.findById2(id);

		//ユーザー情報をリクエストスコープにセットしてjspにフォワード
		request.setAttribute("ad", ad);

		RequestDispatcher dispatcher = request.getRequestDispatcher(EChelper.ADMINISTRATORDELETE_PAGE);
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String id = request.getParameter("id");

		//idを引数にして、idに紐づくユーザー情報を出力する
		AdministratorDAO adao = new AdministratorDAO();
		AdministratorDataBeans ad = adao.delete(id);

		//ユーザー情報をリクエストスコープにセット
		request.setAttribute("ad", ad);

		//削除に成功した場合、管理者一覧画面に遷移
		response.sendRedirect("administratorList");
	}

}
