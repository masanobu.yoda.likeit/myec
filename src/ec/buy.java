package ec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.DeliveryMethodDataBeans;
import beans.ItemDataBeans;
import dao.DeliveryMethodDAO;

/**
 * Servlet implementation class buy
 */
@WebServlet("/buy")
public class buy extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// TODO ログインセッションがない場合、ログイン画面にリダイレクトさせる
		Object UserInfo;
		HttpSession session = request.getSession();

		try {
		ArrayList<ItemDataBeans> cart = (ArrayList<ItemDataBeans>) session.getAttribute("cart");

		UserInfo=(Object)session.getAttribute("userInfo");
	    if(UserInfo==null) {
		      response.sendRedirect("login");
			  return;

	    }else if(cart.size() == 0) {
            request.setAttribute("cartActionMessage", "購入する商品がありません");
	        request.getRequestDispatcher(EChelper.CART_PAGE).forward(request, response);

	    }else {
	    	//配送方法をDBから取得
	    	ArrayList<DeliveryMethodDataBeans> dMDBList  = DeliveryMethodDAO.getAllDeliveryMethodDataBeans();
	        request.setAttribute("dmdbList", dMDBList);
	        request.getRequestDispatcher(EChelper.BUY_PAGE).forward(request, response);
	    }
} catch (Exception e) {
	e.printStackTrace();
	session.setAttribute("errorMessage", e.toString());
	response.sendRedirect("Error");
}}}