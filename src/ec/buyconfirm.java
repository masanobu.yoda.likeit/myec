package ec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.DeliveryMethodDataBeans;
import beans.ItemDataBeans;
import beans.UserDataBeans;
import dao.DeliveryMethodDAO;

/**
 * Servlet implementation class buyconfirm
 */
@WebServlet("/buyconfirm")
public class buyconfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// TODO ログインセッションがない場合、ログイン画面にリダイレクトさせる
	    Object UserInfo;
		HttpSession session = request.getSession();
		UserInfo=(Object)session.getAttribute("userInfo");
	    if(UserInfo==null) {
		     response.sendRedirect("login");
	      	 return;
		}

		request.getRequestDispatcher(EChelper.BUY_CONFIRM_PAGE).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UserDataBeans UserInfo;
		HttpSession session = request.getSession();
		UserInfo=(UserDataBeans)session.getAttribute("userInfo");
        try {
	    //選択された配送方法IDを取得
		int inputDeliveryMethodId = Integer.parseInt(request.getParameter("delivery_method_id"));

		//選択されたIDをもとに配送方法beansを取得
		DeliveryMethodDataBeans userSelectDMB = DeliveryMethodDAO.getDeliveryMethodDataBeansByID(inputDeliveryMethodId);

		//買い物かご
		ArrayList<ItemDataBeans> cartIDBList = (ArrayList<ItemDataBeans>) session.getAttribute("cart");

		//合計金額
		int totalPrice = EChelper.getTotalItemPrice(cartIDBList);

		BuyDataBeans bdb = new BuyDataBeans();
        bdb.setUserId(UserInfo.getId());
        bdb.setTotalPrice(totalPrice);
        bdb.setDeliveryMethodId(userSelectDMB.getId());
        bdb.setDeliveryMethodName(userSelectDMB.getName());
        bdb.setDeliveryMethodPrice(userSelectDMB.getPrice());

        session.setAttribute("bdb", bdb);

		// JSPへフォワード
    	request.getRequestDispatcher(EChelper.BUY_CONFIRM_PAGE).forward(request, response);

        } catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
	}
	}
}