package ec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.BuyDetailDataBeans;
import beans.ItemDataBeans;
import dao.BuyDAO;
import dao.BuyDetailDAO;

/**
 * Servlet implementation class buyresult
 */
@WebServlet("/buyresult")
public class buyresult extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// TODO ログインセッションがない場合、ログイン画面にリダイレクトさせる
	    Object UserInfo;
		HttpSession session = request.getSession();
		UserInfo=(Object)session.getAttribute("userInfo");
	    if(UserInfo==null) {
		     response.sendRedirect("login");
	      	 return;
		}

		// JSPへフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher(EChelper.BUY_RESULT_PAGE);
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		try {

			//セッションからカート情報を取得
			ArrayList<ItemDataBeans> cart = (ArrayList<ItemDataBeans>)session.getAttribute("cart");

			BuyDataBeans bdb = (BuyDataBeans)session.getAttribute("bdb");

			//購入情報を登録
			int buyId = BuyDAO.insertBuy(bdb);

			session.removeAttribute("bdb");
			session.removeAttribute("cart");

			//購入詳細情報を購入情報IDに紐づけして登録
			for(ItemDataBeans cartInItem : cart) {
				BuyDetailDataBeans bddb = new BuyDetailDataBeans();
				bddb.setBuyId(buyId);
				bddb.setItemId(cartInItem.getId());
				BuyDetailDAO.insertBuyDetail(bddb);
			}

            /*==購入完了ページ表示用==*/
			BuyDataBeans resultBDB = BuyDAO.getBuyDataBeansById(buyId);
            request.setAttribute("resultBDB", resultBDB);

            //購入アイテムページ
            ArrayList<ItemDataBeans> buyIDBList = BuyDetailDAO.getItemDataBeansListByBuyId(buyId);
            request.setAttribute("buyIDBList", buyIDBList);

			// JSPへフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher(EChelper.BUY_RESULT_PAGE);
			dispatcher.forward(request, response);

	}catch (Exception e) {
		e.printStackTrace();
		session.setAttribute("errorMessage", e.toString());
		response.sendRedirect("Error");
	}
		}
}
