package ec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;

/**
 * Servlet implementation class cart
 */
@WebServlet("/cart")
public class cart extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Object UserInfo;
		HttpSession session = request.getSession();
		UserInfo=(Object)session.getAttribute("userInfo");
	    if(UserInfo==null) {
		      response.sendRedirect("login");
			  return;
		}

		ArrayList<ItemDataBeans> cart = (ArrayList<ItemDataBeans>) session.getAttribute("cart");

		if(cart==null) {
			cart = new ArrayList<ItemDataBeans>();
			session.setAttribute("cart",cart);
		}

		String cartActionMessage ="";

		if(cart.size()==0) {
			cartActionMessage = "カートに商品がありません";
		}

		request.setAttribute("cartActionMessage", cartActionMessage);
		request.getRequestDispatcher(EChelper.CART_PAGE).forward(request, response);
	}

}
