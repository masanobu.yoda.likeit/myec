package ec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import dao.ItemDAO;

/**
 * Servlet implementation class index
 */
@WebServlet("/index")
public class index extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// TODO ログインセッションがない場合、ログイン画面にリダイレクトさせる
		Object UserInfo;
		HttpSession session = request.getSession();
		UserInfo=(Object)session.getAttribute("userInfo");
	    if(UserInfo==null) {
		      response.sendRedirect("login");
			  return;
		}

	    try {

	    //商品情報を取得
	    ArrayList<ItemDataBeans> itemList = ItemDAO.getRandItem(4);

	    //リクエストスコープにセット
	    request.setAttribute("itemList", itemList);

	    String searchWord = (String)session.getAttribute("searchWord");
	    if(searchWord != null) {
	    	session.removeAttribute("searchWord");
	    }

		//TOPページのjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher(EChelper.INDEX_PAGE);
		dispatcher.forward(request, response);

	    }catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
	}
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/*文字化け対策*/
		request.setCharacterEncoding("UTF-8");

		//TOPページのjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher(EChelper.INDEX_PAGE);
		dispatcher.forward(request, response);
	}

}
