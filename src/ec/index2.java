package ec;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import dao.ItemDAO;

/**
 * Servlet implementation class index2
 */
@WebServlet("/index2")
public class index2 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// TODO ログインセッションがない場合、ログイン画面にリダイレクトさせる
		Object UserInfo;
		HttpSession session = request.getSession();
		UserInfo=(Object)session.getAttribute("userInfo");
	    if(UserInfo==null) {
		     response.sendRedirect("login2");
	    	 return;
		}

		//アイテム一覧情報を取得
		ItemDAO itemdao = new ItemDAO();
        List<ItemDataBeans> iList = itemdao.findAll();

        //リクエストスコープにアイテム一覧情報をセット
        request.setAttribute("iList", iList);

		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher(EChelper.INDEX_PAGE2);
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //検索処理全般
		request.setCharacterEncoding("UTF-8");

		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String price = request.getParameter("price");
		String price2 = request.getParameter("price2");
		String detail = request.getParameter("detail");

		ItemDAO itemdao = new ItemDAO();
		List<ItemDataBeans> ilist = itemdao.search(id,name,price,price2,detail);

		request.setAttribute("id", id);
		request.setAttribute("name", name);
		request.setAttribute("price", price);
		request.setAttribute("price2", price2);
		request.setAttribute("detail", detail);

		/**テーブルに該当のデータが見つかった時**/
		//リクエストスコープにアイテム一覧データをセット
        request.setAttribute("iList", ilist);

		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher(EChelper.INDEX_PAGE2);
		dispatcher.forward(request, response);
	}

}
