package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import dao.ItemDAO;

/**
 * Servlet implementation class item
 */
@WebServlet("/item")
public class item extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// TODO ログインセッションがない場合、ログイン画面にリダイレクトさせる
		Object UserInfo;
		HttpSession session = request.getSession();
		UserInfo=(Object)session.getAttribute("userInfo");
	    if(UserInfo==null) {
		   response.sendRedirect("login");
		   return;
		}

	    //選択された商品のIDを型変換し使用
	    int id = Integer.parseInt(request.getParameter("item_id"));

	    //戻るページ選択用
	    int pageNum = Integer.parseInt(request.getParameter("page_num")==null?"1":request.getParameter("page_num"));

	    //対象のアイテム情報を取得
	    ItemDataBeans item = ItemDAO.getItemByItemID(id);

	    //リクエストパラメーターにセット
	    request.setAttribute("item", item);
        request.setAttribute("pageNum", pageNum);

		// JSPへフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher(EChelper.ITEM_PAGE);
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// JSPへフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher(EChelper.ITEM_PAGE);
		dispatcher.forward(request, response);
	}

}
