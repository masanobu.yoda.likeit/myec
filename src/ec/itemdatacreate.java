package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.ItemDAO;

/**
 * Servlet implementation class itemdatacreate
 */
@WebServlet("/itemdatacreate")
public class itemdatacreate extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO ログインセッションがない場合、ログイン画面にリダイレクトさせる
				Object UserInfo;
				HttpSession session = request.getSession();
			    UserInfo=(Object)session.getAttribute("userInfo");
				if(UserInfo==null) {
				  response.sendRedirect("login2");
				  return;
				}

		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher(EChelper.ITEMDATACREATE_PAGE);
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		//リクエストパラメータの入力項目を取得
		String name = request.getParameter("name");
		String price = request.getParameter("price");
		String detail = request.getParameter("detail");
		String filename = request.getParameter("file_name");

		/**登録失敗**/
		if(name.equals("") || price.equals("") || detail.equals("") || filename.equals("")) {
			//リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			request.setAttribute("name", name);
			request.setAttribute("price", price);
			request.setAttribute("detail", detail);
			request.setAttribute("filename", filename);

			//新規登録jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher(EChelper.ITEMDATACREATE_PAGE);
			dispatcher.forward(request, response);
			return;
		}

		/**登録成功**/
		//リクエストパラメータの入力項目を引数に渡して、DAOのメソッドを実行
		ItemDAO itemdao = new ItemDAO();
		itemdao.insert(name,detail,price,filename);

		//アイテム一覧のサーブレットにリダイレクト
		response.sendRedirect("index2");
	}

}
