package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import dao.ItemDAO;

/**
 * Servlet implementation class itemdatadelete
 */
@WebServlet("/itemdatadelete")
public class itemdatadelete extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO ログインセッションがない場合、ログイン画面にリダイレクトさせる
				Object UserInfo;
				HttpSession session = request.getSession();
			    UserInfo=(Object)session.getAttribute("userInfo");
				if(UserInfo==null) {
				  response.sendRedirect("login2");
				  return;
				}



		//URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");

		//idを引数にして、idに紐づくユーザー情報を出力する
		ItemDAO itemdao = new ItemDAO();
		ItemDataBeans IDB = itemdao.findById2(id);

		//アイテムデータをリクエストスコープにセットしてjspにフォワード
		request.setAttribute("IDB", IDB);

		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher(EChelper.ITEMDATAUPDELETE_PAGE);
		dispatcher.forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		//URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");

		//idを引数にして、idに紐づくユーザー情報を出力する
		ItemDAO itemdao = new ItemDAO();
		ItemDataBeans IDB = itemdao.delete(id);

		//アイテムデータをリクエストスコープにセット
		request.setAttribute("IDB", IDB);

		//削除に成功した場合、アイテム一覧画面にフォワード
		response.sendRedirect("index2");
	}

}
