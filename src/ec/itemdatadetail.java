package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import dao.ItemDAO;

/**
 * Servlet implementation class itemdatadetail
 */
@WebServlet("/itemdatadetail")
public class itemdatadetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO ログインセッションがない場合、ログイン画面にリダイレクトさせる
				Object UserInfo;
				HttpSession session = request.getSession();
			    UserInfo=(Object)session.getAttribute("userInfo");
				if(UserInfo==null) {
				  response.sendRedirect("login2");
				  return;
				}




		//URLからGETパラメータとしてidを受け取る
		String id = request.getParameter("id");

		//idを引数にして、idに紐づくユーザー情報を出力する
		ItemDAO itemdao = new ItemDAO();
		ItemDataBeans ID = itemdao.findById(id);

		//ユーザー情報をリクエストスコープにセットしてjspにフォワード
		request.setAttribute("ID", ID);

		RequestDispatcher dispatcher = request.getRequestDispatcher(EChelper.ITEMDATADETAIL_PAGE);
		dispatcher.forward(request, response);
	}



}
