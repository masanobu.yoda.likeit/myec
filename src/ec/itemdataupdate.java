package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import dao.ItemDAO;

/**
 * Servlet implementation class itemdataupdate
 */
@WebServlet("/itemdataupdate")
public class itemdataupdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// TODO ログインセッションがない場合、ログイン画面にリダイレクトさせる
		Object UserInfo;
		HttpSession session = request.getSession();
		UserInfo=(Object)session.getAttribute("userInfo");
		if(UserInfo==null) {
			  response.sendRedirect("login2");
			  return;
		}

		//URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");

		//idを引数にして、idに紐づくユーザー情報を出力する
		ItemDAO itemdao = new ItemDAO();
		ItemDataBeans ID = itemdao.findById(id);

		//ユーザー情報をリクエストスコープにセットしてjspにフォワード
		request.setAttribute("ID", ID);

		RequestDispatcher dispatcher = request.getRequestDispatcher(EChelper.ITEMDATAUPDATE_PAGE);
		dispatcher.forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//リクエストパラメータの文字コード指定
		request.setCharacterEncoding("UTF-8");

		//リクエストパラメータの入力項目を取得
		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String price = request.getParameter("price");
		String detail = request.getParameter("detail");
		String filename = request.getParameter("file_name");

		/**更新失敗**/
		if(name.equals("") || price.equals("") || detail.equals("") || filename.equals("") || id.equals("")) {
			//リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			//idを引数にして、idに紐づくユーザー情報を出力する
			ItemDAO itemdao = new ItemDAO();
			ItemDataBeans ID = itemdao.findById(id);

			//アイテムデータをリクエストスコープにセット
			request.setAttribute("ID", ID);

			//アイテムデータ更新画面jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher(EChelper.ITEMDATAUPDATE_PAGE);
			dispatcher.forward(request, response);
			return;
		}

		/**更新成功**/
		//リクエストパラメータの入力項目を引数に渡してDAOのメソッドを実行
		ItemDAO itemdao = new ItemDAO();
        itemdao.update(name, price, detail, filename,id);

		//フォワード
		response.sendRedirect("index2");
	}

}
