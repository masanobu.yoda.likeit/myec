package ec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import dao.ItemDAO;

/**
 * Servlet implementation class itemsearchresult
 */
@WebServlet("/itemsearchresult")
public class itemsearchresult extends HttpServlet {
	private static final long serialVersionUID = 1L;

	//1頁に表示する商品数
	final static int PAGE_MAX_ITEM_COUNT = 8;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// TODO ログインセッションがない場合、ログイン画面にリダイレクトさせる
		Object UserInfo;
		HttpSession session = request.getSession();
		UserInfo=(Object)session.getAttribute("userInfo");
	    if(UserInfo==null) {
		   response.sendRedirect("login");
		   return;
		}

	    try {

        String searchWord = request.getParameter("search_word");

		//表示ページ番号　未指定の場合1頁目を表示
		int pageNum = Integer.parseInt(request.getParameter("page_num") == null ? "1" : request.getParameter("page_num"));

		//新たに検索されたキーワードをセッションに格納する。
		session.setAttribute("searchWord",searchWord);

		//商品リストを作成　ページ表示分のみ
		ArrayList<ItemDataBeans> searchResultItemList = ItemDAO.getItemByItemName(searchWord,pageNum,PAGE_MAX_ITEM_COUNT);

		//検索ワードに対して総ページ数を取得
		double itemCount = ItemDAO.getItemCount(searchWord);

		int pageMax = (int) Math.ceil(itemCount/PAGE_MAX_ITEM_COUNT);

		//総アイテム数
		request.setAttribute("itemCount",(int)itemCount);

		//総ページ数
		request.setAttribute("pageMax",pageMax);

		//表示ページ
		request.setAttribute("pageNum",pageNum);
		request.setAttribute("itemList",searchResultItemList);

		// JSPへフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher(EChelper.ITEM_SEARCH_RESULT_PAGE);
		dispatcher.forward(request, response);

	    } catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
	}

	    }
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/*文字化け対策*/
		request.setCharacterEncoding("UTF-8");

		//TOPページのjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher(EChelper.ITEM_SEARCH_RESULT_PAGE);
		dispatcher.forward(request, response);
	}
}
