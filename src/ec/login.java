package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;

/**
 * ログイン画面
 */
@WebServlet("/login")
public class login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher(EChelper.LOGIN_PAGE);
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/*文字化け対策*/
		request.setCharacterEncoding("UTF-8");

		//リクエストパラメータの入力項目を取得
		String loginId = request.getParameter("login_id");
		String password = request.getParameter("password");

		//リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		UserDAO userDao = new UserDAO();
		UserDataBeans user = userDao.findByLoginInfo(loginId,password);

		/**テーブルに該当のデータが見つからなかった場合**/
		if(user == null) {
			//リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力内容が正しくありません。");

			//ログインjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher(EChelper.LOGIN_PAGE);
			dispatcher.forward(request, response);
			return;
		}

		/**テーブルに該当のデータが見つかった場合**/
		//セッションにユーザのデータをセット
		HttpSession session = request.getSession();
		session.setAttribute("userInfo", user);


		//TOPページにリダイレクト
		response.sendRedirect("index");



}
}
