package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.AdministratorDataBeans;
import dao.AdministratorDAO;

/**
 * Servlet implementation class login2
 */
@WebServlet("/login2")
public class login2 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher(EChelper.LOGIN_PAGE2);
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//リクエストパラメータの文字コード指定
		request.setCharacterEncoding("UTF-8");

		//リクエストパラメータの入力項目を取得
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");

		//リクエストパラメータの入力項目を引数に渡して、DAOのメソッドを実行
		AdministratorDAO adao = new AdministratorDAO();
		AdministratorDataBeans user = adao.findByLoginInfo(loginId, password);

		/**テーブルに該当のデータが見つからなかった時**/
		if(user==null) {
			//リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "ログインに失敗しました。");

            //ログインjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher(EChelper.LOGIN_PAGE2);
			dispatcher.forward(request, response);
			return;
		}

		/**テーブルに該当のデータが見つかった時**/
		//セッションにユーザーの情報をセット
		HttpSession session = request.getSession();
		session.setAttribute("userInfo",user);

		//管理者用TOPページのサーブレットにリダイレクト
		response.sendRedirect("index2");
	}

}
