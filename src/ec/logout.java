package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class logout
 */
@WebServlet("/logout")
public class logout extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();

		//ログイン時に保存したセッション内のユーザー情報を削除
		session.removeAttribute("userInfo");

		//ログインのサーブレットにリダイレクト
		RequestDispatcher dispatcher = request.getRequestDispatcher(EChelper.LOGOUT_PAGE);
		dispatcher.forward(request, response);
	}



}
