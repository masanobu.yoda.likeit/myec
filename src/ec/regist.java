package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserDataBeans;
import dao.UserDAO;



/**
 * Servlet implementation class regist
 */
@WebServlet("/regist")
public class regist extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher(EChelper.REGIST_PAGE);
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		//リクエストパラメータの入力項目を取得
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String passwordc = request.getParameter("passwordConf");
		String name = request.getParameter("name");
		String birthdate = request.getParameter("birthdate");

		UserDAO userdao = new UserDAO();
		UserDataBeans user = userdao.findByLoginId(loginId);

		/**登録失敗**/
		if( !(password.equals(passwordc)) || loginId.equals("") || name.equals("") || birthdate.equals("") || password.equals("") || user != null) {
			//リクエストスコープにエラーメッセージをセット

			if(user != null) {
				request.setAttribute("errMsg", "ログインIDが重複しています");

				request.setAttribute("name", name);
				request.setAttribute("birthdate", birthdate);

				RequestDispatcher dispatcher = request.getRequestDispatcher(EChelper.REGIST_PAGE);
				dispatcher.forward(request, response);
				return;

			}else {

			request.setAttribute("errMsg", "入力された内容は正しくありません");

			request.setAttribute("loginId",loginId );
			request.setAttribute("name", name);
			request.setAttribute("birthdate", birthdate);

			//新規登録jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher(EChelper.REGIST_PAGE);
			dispatcher.forward(request, response);
			return;
		}}

		/**登録成功**/
		//リクエストパラメータの入力項目を引数に渡して、DAOのメソッドを実行
        UserDAO userDao = new UserDAO();
        userDao.insert(loginId, password, name, birthdate);

		//確認画面のサーブレットにリダイレクト
		response.sendRedirect("login");

		}
	}


