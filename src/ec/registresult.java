package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class registresult
 */
@WebServlet("/registresult")
public class registresult extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TOPページのjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher(EChelper.REGIST_RESULT_PAGE);
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/*文字化け対策*/
		request.setCharacterEncoding("UTF-8");

		//TOPページのjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher(EChelper.REGIST_RESULT_PAGE);
		dispatcher.forward(request, response);
	}

}
