package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;

/**
 * Servlet implementation class taikai
 */
@WebServlet("/taikai")
public class taikai extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//セッション開始
		HttpSession session = request.getSession();

		Object UserInfo;
		UserInfo=(Object)session.getAttribute("userInfo");
		if(UserInfo==null) {
			response.sendRedirect("login");
			return;
		}

		//ログイン時に取得したユーザーIDをセッションから取得
		UserDataBeans userInfo = (UserDataBeans)session.getAttribute("userInfo");

		// URLからGETパラメータとしてIDを受け取る
		String id = Integer.toString(userInfo.getId());

		//idを引数にして、idに紐づくユーザー情報を出力する
		UserDAO userDao = new UserDAO();
		UserDataBeans user = userDao.findById(id);

		//ユーザー情報をリクエストスコープにセットしてjspにフォワード
		request.setAttribute("userList", user);



		//ログインのサーブレットにリダイレクト
		RequestDispatcher dispatcher = request.getRequestDispatcher(EChelper.TAIKAI_PAGE);
		dispatcher.forward(request, response);

	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");

			String id = request.getParameter("id");

			//idを引数にして、idに紐づくユーザー情報を出力する
			UserDAO userDao = new UserDAO();
			UserDataBeans user3 = userDao.delete(id);

			//ユーザー情報をリクエストスコープにセット
			request.setAttribute("userList", user3);

            session.removeAttribute("userInfo");

			//削除に成功した場合、ログイン画面に遷移する
			response.sendRedirect("login");
	}

}
