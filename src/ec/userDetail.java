package ec;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.BuyDataBeans;
import beans.UserDataBeans;
import dao.BuyDAO;
import dao.UserDAO;

/**
 * Servlet implementation class userDetail
 */
@WebServlet("/userDetail")
public class userDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// URLからGETパラメータとしてIDを受け取る
        String id = request.getParameter("id");
        int ID = Integer.parseInt(id);

		//idを引数にして、idに紐づくユーザー情報を出力する
        UserDAO userDao = new UserDAO();
        UserDataBeans user = userDao.findById(id);

		//ユーザー情報をリクエストスコープにセットしてjspにフォワード
		request.setAttribute("user",user);

		//購入履歴を取得
        BuyDAO buyDao = new BuyDAO();
        ArrayList<BuyDataBeans> bdList = null;
		try {
			bdList = buyDao.findAll(ID);
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

        //リクエストスコープに購入履歴をセット
        request.setAttribute("bdList", bdList);



		//TOPページのjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher(EChelper.USERDETAIL_PAGE);
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/*文字化け対策*/
		request.setCharacterEncoding("UTF-8");

		//TOPページのjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher(EChelper.USERDETAIL_PAGE);
		dispatcher.forward(request, response);
	}

}


