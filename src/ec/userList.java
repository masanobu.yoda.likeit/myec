package ec;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;


/**
 * Servlet implementation class userList
 */
@WebServlet("/userList")
public class userList extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// TODO ログインセッションがない場合、ログイン画面にリダイレクトさせる
		Object UserInfo;
		HttpSession session = request.getSession();
		UserInfo=(Object)session.getAttribute("userInfo");
	    if(UserInfo==null) {
		     response.sendRedirect("login2");
	    	 return;
		}

		//ユーザー一覧情報を取得
		UserDAO userdao = new UserDAO();
        List<UserDataBeans> userList = userdao.findAll();

        //リクエストスコープにユーザー一覧情報をセット
        request.setAttribute("userList", userList);

		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher(EChelper.USERLIST_PAGE);
		dispatcher.forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//検索処理全般
		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		String birthdate = request.getParameter("birthdate");
		String birthdate2 = request.getParameter("birthdate2");

		UserDAO userdao = new UserDAO();
        List<UserDataBeans> userList = userdao.search(loginId,name,birthdate,birthdate2);

        request.setAttribute("loginId", loginId);
        request.setAttribute("name", name);
        request.setAttribute("birthdate", birthdate);
        request.setAttribute("birthdate2", birthdate2);

        /**テーブルに該当のデータが見つかった時**/
        //リクエストスコープにユーザー一覧情報をセット
        request.setAttribute("userList", userList);

		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher(EChelper.USERLIST_PAGE);
		dispatcher.forward(request, response);
	}

}
