package ec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.BuyDetailDataBeans;
import beans.ItemDataBeans;
import dao.BuyDAO;
import dao.BuyDetailDAO;

/**
 * Servlet implementation class userbuyhistorydelete
 */
@WebServlet("/userbuyhistorydelete")
public class userbuyhistorydelete extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		try {
			String buyId = request.getParameter("buy_id");
		    int i = Integer.parseInt(buyId);

			BuyDataBeans resultBDB = BuyDAO.getBuyDataBeansById(i);
		    request.setAttribute("resultBDB", resultBDB);

		    ArrayList<ItemDataBeans> buyIDBList = BuyDetailDAO.getItemDataBeansListByBuyId(i);
		    request.setAttribute("buyIDBList", buyIDBList);

		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher(EChelper.USERBUYHISTORY_DELETE_PAGE);
		dispatcher.forward(request, response);



		}catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
	}
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		try {
			String buyId = request.getParameter("buy_id");
		    int i = Integer.parseInt(buyId);

			BuyDataBeans resultBDB = BuyDAO.getBuyDataBeansDelete(i);
		    request.setAttribute("resultBDB", resultBDB);

		    BuyDetailDataBeans buyIDBList = BuyDetailDAO.getItemDataBeansListDelete(i);
		    request.setAttribute("buyIDBList", buyIDBList);

		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher(EChelper.USERBUYHISTORY_DELETE_PAGE);
		dispatcher.forward(request, response);



		}catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
	}

}}
