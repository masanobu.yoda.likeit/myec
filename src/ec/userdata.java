package ec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.UserDataBeans;
import dao.BuyDAO;
import dao.UserDAO;

/**
 * Servlet implementation class userdata
 */
@WebServlet("/userdata")
public class userdata extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//セッション開始
		HttpSession session = request.getSession();

		Object UserInfo;
		UserInfo=(Object)session.getAttribute("userInfo");
		if(UserInfo==null) {
			response.sendRedirect("login");
			return;
		}

		try {

	    //ログイン時に取得したユーザーIDをセッションから取得
		UserDataBeans userInfo = (UserDataBeans)session.getAttribute("userInfo");

		//購入履歴を取得
        BuyDAO buyDao = new BuyDAO();
        ArrayList<BuyDataBeans> bdList = buyDao.findAll(userInfo.getId());

        //リクエストスコープに購入履歴をセット
        request.setAttribute("bdList", bdList);

		//ユーザー情報のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher(EChelper.USERDATA_PAGE);
		dispatcher.forward(request, response);


		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
	}}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//セッション開始
		HttpSession session = request.getSession();
		//リクエストパラメータの文字コード指定
		request.setCharacterEncoding("UTF-8");

		try {
		//リクエストパラメータの入力項目を取得
		String id = request.getParameter("id");
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String passwordc = request.getParameter("passwordConf");
		String name = request.getParameter("name");
		String birthdate = request.getParameter("birthdate");

		UserDAO userdao = new UserDAO();
		UserDataBeans User = userdao.findByLoginId(loginId);

		/**更新失敗**/
		if( !(password.equals(passwordc)) ||  name.equals("") || birthdate.equals("") ||password.equals("") || User != null) {
            if(User != null) {
            	request.setAttribute("errMsg", "ログインIDが重複しています");

            	//ログイン時に取得したユーザーIDをセッションから取得
    			UserDataBeans userInfo = (UserDataBeans)session.getAttribute("userInfo");

    			//購入履歴を取得
    	        BuyDAO buyDao = new BuyDAO();
    	        ArrayList<BuyDataBeans> bdList = buyDao.findAll(userInfo.getId());

    	        //リクエストスコープに購入履歴をセット
    	        request.setAttribute("bdList", bdList);

    			//ユーザー情報更新画面jspにフォワード
    			RequestDispatcher dispatcher = request.getRequestDispatcher(EChelper.USERDATA_PAGE);
    			dispatcher.forward(request, response);
    			return;
            }

			//リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			//ログイン時に取得したユーザーIDをセッションから取得
			UserDataBeans userInfo = (UserDataBeans)session.getAttribute("userInfo");

			//購入履歴を取得
	        BuyDAO buyDao = new BuyDAO();
	        ArrayList<BuyDataBeans> bdList = buyDao.findAll(userInfo.getId());

	        //リクエストスコープに購入履歴をセット
	        request.setAttribute("bdList", bdList);

			//ユーザー情報更新画面jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher(EChelper.USERDATA_PAGE);
			dispatcher.forward(request, response);
			return;
		}

	    /**更新成功**/
        //リクエストパラメータの入力項目を引数に渡してDaoのメソッドを実行
        UserDAO userDao = new UserDAO();
        userDao.update(password,name,birthdate,loginId,id);

        request.setAttribute("errMsg", "更新成功しました。");


        UserDataBeans user = userDao.findByLoginInfo(loginId, password);
        session.setAttribute("userInfo", user);


        doGet(request, response);

		}catch (Exception e) {
		e.printStackTrace();
		session.setAttribute("errorMessage", e.toString());
		response.sendRedirect("Error");
}}}


