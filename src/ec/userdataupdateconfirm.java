package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class userdataupdateconfirm
 */
@WebServlet("/userdataupdateconfirm")
public class userdataupdateconfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//文字化け対策
		request.setCharacterEncoding("UTF-8");

		//ユーザー情報のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher(EChelper.USERDATA_UPDATE_CONFIRM_PAGE);
		dispatcher.forward(request, response);
	}

}
